<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


function ea_translate($str, $str_plurals = null, $number = null)
{


    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('event_appointment');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}



/**
 * @return ea_Controller
 */
function ea_Controller()
{
    require_once dirname(__FILE__).'/controller.class.php';
    return bab_getInstance('ea_Controller');
}

/**
 * @return ea_AddressSet
 */
function ea_AddressSet()
{
    require_once dirname(__FILE__).'/set/address.class.php';

    return new ea_AddressSet();
}


/**
 * @return ea_ContactSet
 */
function ea_ContactSet()
{
    require_once dirname(__FILE__).'/set/contact.class.php';

    return new ea_ContactSet();
}


/**
 * @return ea_CountrySet
 */
function ea_CountrySet()
{
    require_once dirname(__FILE__).'/set/country.class.php';

    return new ea_CountrySet();
}


/**
 * @return ea_CategorySet
 */
function ea_CategorySet()
{
    require_once dirname(__FILE__).'/set/category.class.php';

    return new ea_CategorySet();
}


/**
 * @return ea_TypeSet
 */
function ea_TypeSet()
{
    require_once dirname(__FILE__).'/set/type.class.php';

    return new ea_TypeSet();
}


/**
 * @return ea_EventSet
 */
function ea_EventSet()
{
    require_once dirname(__FILE__).'/set/event.class.php';

    return new ea_EventSet();
}



function ea_getAccessibleCategories()
{
    static $return = null;

    if($return !== null){
        return $return;
    }

    $return = array();
    $set = ea_CategorySet();
    $categories = $set->select();
    foreach($categories as $category)
    {
        if(bab_isAccessValid('ea_eventaccess_groups', $category->id)){
            $return[$category->id] = $category->id;
        }
    }

    return $return;
}


function ea_getAccessibleEvents($idEvent = null)
{
    static $return = null;

    if($return !== null){
        if($idEvent){
            return isset($return[$idEvent]);
        }
        return $return;
    }

    $categories = ea_getAccessibleCategories();
    $return = array();
    $set = ea_EventSet();
    $events = $set->select();
    foreach($events as $event)
    {
        if(isset($categories[$event->category])){
            $return[$event->id] = $event->id;
        }
    }
    if($idEvent){
        return isset($return[$idEvent]);
    }
    return $return;
}


function ea_Page()
{
    $addon = bab_getAddonInfosInstance('event_appointment');

    $W = bab_Widgets();
    $page = $W->BabPage();

    if($_SERVER['HTTP_HOST'] == 'localhost'){
        $page->addStyleSheet('../../'.$addon->getStylePath().'event_appointment.css');
    }else{
        $page->addStyleSheet($addon->getStylePath().'event_appointment.css');
    }


    return $page;
}


function ea_date($date)
{
    $date = explode('-', $date);

    return $date[2].'-'.$date[1].'-'.$date[0];
}

