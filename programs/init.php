<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';





function event_appointment_upgrade($version_base, $version_ini)
{
    global $babDB;

    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    require_once $GLOBALS['babInstallPath'] . "utilit/eventincl.php";

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet(ea_CountrySet());
    $synchronize->addOrmSet(ea_AddressSet());
    $synchronize->addOrmSet(ea_ContactSet());
    $synchronize->addOrmSet(ea_CategorySet());
    $synchronize->addOrmSet(ea_TypeSet());
    $synchronize->addOrmSet(ea_EventSet());
    $synchronize->updateDatabase();

    $countrySet = ea_CountrySet();
    if ($synchronize->isCreatedTable('ea_country') || 0 === $countrySet->select()->count()) {
        if ($countrySet->populateFromGeoNames()) {
            bab_installWindow::message(bab_toHtml(ea_translate('Countries database initialization... done')));
        }
    }

    $synchronize = new bab_synchronizeSql();
    $synchronize->setDisplayMessage(false);
    $synchronize->fromSqlFile(dirname(__FILE__).'/data/eventaccess.sql');
    $synchronize->fromSqlFile(dirname(__FILE__).'/data/access.sql');

    if ($synchronize->isEmpty('ea_access_groups')) {

        // set administrators as default group for aincrm staff

        include_once $GLOBALS['babInstallPath']."admin/acl.php";
        aclAdd('ea_access_groups', BAB_ADMINISTRATOR_GROUP, 1);
    }

    $addon = bab_getAddonInfosInstance('event_appointment');
    if (method_exists($addon, 'addEventListener')) {
    	bab_removeAddonEventListeners('event_appointment');
        // support for standard addon location in vendor
        $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'event_appointment_onBeforeSiteMapCreated', 'events.php');
        $addon->addEventListener('bab_eventBeforePeriodsCreated', 'event_appointment_onBeforePeriodsCreated', 'events.php');
        $addon->addEventListener('bab_eventCollectCalendarsBeforeDisplay', 'event_appointment_onCollectCalendarsBeforeDisplay', 'events.php');
    }else{
         // old ovidentia before 8.2.0
        bab_addEventListener(
            'bab_eventBeforeSiteMapCreated',
            'event_appointment_onBeforeSiteMapCreated',
            $addon->getRelativePath().'events.php',
            'event_appointment'
        );
        bab_addEventListener(
            'bab_eventBeforePeriodsCreated',
            'event_appointment_onBeforePeriodsCreated',
            $addon->getRelativePath().'events.php',
            'event_appointment'
        );
        bab_addEventListener(
            'bab_eventCollectCalendarsBeforeDisplay',
            'event_appointment_onCollectCalendarsBeforeDisplay',
            $addon->getRelativePath().'events.php',
            'event_appointment'
        );
    }

    $addon->registerFunctionality('Func_CalendarBackend_eventappointment', 'calendar.backend.class.php');

    return true;
}


/*

function event_appointment_onPageRefreshed()
{
    if ('event_appointment' === $GLOBALS['babSkin']) {
        $jquery = bab_functionality::get('jquery');
        $jquery->includeCore();

        global $babBody;

        $addon = bab_getAddonInfosInstance('event_appointment');
        //$babBody->addJavascriptFile($addon->getPhpPath().'jquery.dropmenu.js');
    }
}*/

