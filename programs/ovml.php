<?php
/**
 * <OCAddon name="event_appointment" action="getEvents" [category="id"] [start="Y-m-d"] [end="Y-m-d"] [rows="5"] [sort="ASC|DESC"]>
 *
 * OVEventId 			: contient l'ID de l'événement,
 * OVEventName 			: Contient le titre de l'évément,
 * OVEventDescription	: Contient la description de l'événement,
 * OVEventStartDateTime : contient un timestamp de l'heure et la date de début de l'événement,
 * OVEventEndDateTime 	: contient un timestamp de l'heure et la date de fin de l'évenement,
 * OVEventCategory	 	: contient le nom de la catégorie de l'événement,
 * OVEventAccess		: True/False, en fonction de la possibilité d'inscription à l'évènement,
 * OVEventColor		 	: contient la couleur sans  le #,
 * OVEventType 			: contient le nom du type d'événement,
 * OVEventCity 			: contient la ville de l'événement,
 * OVEventSignup 		: contient 1 si l'inscription est activé sur lévénement,
 * OVEventUrl 			: contient le lien de visualisation de l'événement.
 * OVEventCategoryUrl	: contient le lien de la liste des événement filtré sur la catégorie de cette événément.
 * OVEventEditUrl		: contient le lien d'edition d'un evenement si l'utilisateur peux.
 *
 *
 * <OCAddon name="event_appointment" action="getEvent" [id="idEvent"]>
 *
 * OVEventId 			: contient l'ID de l'événement,
 * OVEventName 			: Contient le titre de l'évément,
 * OVEventDescription	: Contient la description de l'événement,
 * OVEventStartDateTime : contient un timestamp de l'heure et la date de début de l'événement,
 * OVEventEndDateTime 	: contient un timestamp de l'heure et la date de fin de l'évenement,
 * OVEventCategory	 	: contient le nom de la catégorie de l'événement,
 * OVEventAccess		: True/False, en fonction de la possibilité d'inscription à l'évènement,
 * OVEventColor		 	: contient la couleur sans  le #,
 * OVEventType 			: contient le nom du type d'événement,
 * OVEventCity 			: contient la ville de l'événement,
 * OVEventSignup 		: contient 1 si l'inscription est activé sur lévénement,
 * OVEventUrl 			: contient le lien de visualisation de l'événement.
 * OVEventCategoryUrl	: contient le lien de la liste des événement filtré sur la catégorie de cette événément.
 * OVEventEditUrl		: contient le lien d'edition d'un evenement si l'utilisateur peux.
 *
 *
 *
 * <OCAddon name="event_appointment" action="getFiles" [id="idEvent"] [type="public|private(default)"]>
 *
 * OVEventFileName			: Contient le nom du fichier,
 * OVEventFileThumbnail		: Contient le chemin vers un thumbnail du fichier,
 * OVEventFileAccess		: True/False, en fonction de la possibilité d'inscription à l'évènement,
 * OVEventFileType			: public/private,
 * OVEventFileDownloadUrl	: Contient l'url de DL pour le moment les fichiers ne sont accessible qu'au personne pouvant s'inscrire.
 *
 *
 *
 * Test si l'utilisateur pour editer
 * <OCAddon name="event_appointment" action="canManage">
 * <OVAddUrl>
 *
 */

include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';


function event_appointment_ovml($args)
{
    $ovContainer = array();


    if (!isset($args['action']))
    {
        trigger_error('Parameter \'action\' must be specified.');
        return $ovContainer;
    }

    if (!isset($args['rows']))
    {
        $args['rows'] = 5;
    }

    if (!isset($args['start'])) {
        $args['start'] = date('Y-m-d H:i:s');
    }
    if (!isset($args['sort'])) {
        $args['sort'] = 'ASC';
    }
    switch ($args['action'])
    {
        case 'canManage':
            if(bab_isAccessValid('ea_access_groups', 1)){
                $ovContainer[] = array(
                    'AddUrl' => ea_Controller()->Event()->editEvent()->url()
                );
            }
            break;

        case 'getEvents':
            ea_CategorySet();
            ea_TypeSet();
            ea_AddressSet();
            $set = ea_EventSet();
            $set->category();
            $set->type();
            $set->address();


            $conditions = $set->start->greaterThanOrEqual($args['start']);

            if (isset($args['end'])) {
                $conditions = $conditions->_AND_($set->start->lessThan($args['end']));
            }

            /*->_AND_($set->start_time->greaterThanOrEqual($set->start_time->input(date('H:i:s'))))*/;
            if (isset($args['category']) && !empty($args['category'])) {
                $conditions = $conditions->_AND_($set->category->id->is($args['category']));
            }

            $events = $set->select($conditions);
            if ($args['sort'] == 'DESC') {
                $events->orderDesc($set->start);
            } else {
                $events->orderAsc($set->start);
            }

            $editAccess = bab_isAccessValid('ea_access_groups', 1);

            $i = 0;
            foreach($events as $event){
                $access = bab_isAccessValid('ea_eventaccess_groups', $event->category->id);
                $eventEditUrl = '';
                if($editAccess){
                    $eventEditUrl = ea_Controller()->Event()->editEvent($event->id)->url();
                }
                $ovElement = array(
                    'EventId' => $event->id,
                    'EventName' => $event->name,
                    'EventDescription' => $event->description,
                    'EventStartDateTime' => bab_mktime($event->start),
                    'EventEndDateTime' => bab_mktime($event->end),
                    'EventCategory' => $event->category->name,
                    'EventAccess' => (int)$access,
                    'EventColor' => $event->category->color,
                    'EventType' => $event->type->name,
                    'EventCity' => $event->address->city,
                    'EventCity' => $event->address->city,
                    'EventSignup' => $event->signup,
                    'EventUrl' => ea_Controller()->Contact()->signup($event->id)->url(),
                    'EventCategoryUrl' => ea_Controller()->Event()->displayViewList(array('filter' => array('category'=>$event->category->id)))->url(),
                    'EventEditUrl' => $eventEditUrl
                );
                $ovContainer[] = $ovElement;
                $i++;
                if($i == $args['rows']){
                    break;
                }
            }
            break;

        case 'getEvent':
            ea_CategorySet();
            ea_TypeSet();
            ea_AddressSet();
            $set = ea_EventSet();
            $set->category();
            $set->type();
            $set->address();

            if (!isset($args['id'])) {
                return array();
            }

            $conditions = $set->id->is($args['id']);

            $event = $set->get($conditions);

            if (!$event) {
                return array();
            } else {
                $access = bab_isAccessValid('ea_eventaccess_groups', $event->category->id);
                $eventEditUrl = '';
                if(bab_isAccessValid('ea_access_groups', 1)){
                    $eventEditUrl = ea_Controller()->Event()->editEvent($event->id)->url();
                }
                $ovElement = array(
                    'EventId' => $event->id,
                    'EventName' => $event->name,
                    'EventDescription' => $event->description,
                    'EventStartDateTime' => bab_mktime($event->start),
                    'EventEndDateTime' => bab_mktime($event->end),
                    'EventCategory' => $event->category->name,
                    'EventAccess' => (int)$access,
                    'EventColor' => $event->category->color,
                    'EventType' => $event->type->name,
                    'EventCity' => $event->address->city,
                    'EventSignup' => $event->signup,
                    'EventUrl' => ea_Controller()->Contact()->signup($event->id)->url(),
                    'EventCategoryUrl' => ea_Controller()->Event()->displayViewList(array('filter' => array('category'=>$event->category->id)))->url(),
                    'EventEditUrl' => $eventEditUrl
                );
                $ovContainer[] = $ovElement;
            }
            break;

        case 'getFiles':
            $set = ea_EventSet();

            if (!isset($args['id'])) {
                return array();
            }

            if (isset($args['type'])) {
                $type = $args['type'];
            } else {
                $type = 'private';
            }

            $conditions = $set->id->is($args['id']);

            $event = $set->get($conditions);

            if (!$event) {
                return array();
            } else {
                $path = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
                $path->push('event');
                $path->push($event->id);
                if ($type == 'public') {
                    $path->push('public');
                }
                $access = bab_isAccessValid('ea_eventaccess_groups', $event->category);
                foreach ($path as $file) {
                    if($file->isFile()){
                        $filepath = $file->toString();
                        /*@var $T Func_Thumbnailer */
                        $T = @bab_functionality::get('Thumbnailer', false);
                        if ($T) {
                            $icon = $T->getIcon($filepath, 32, 32);
                            if (is_object($icon)) {
                                $icon = $icon->__toString();
                            }

                            $icon;
                        } else {
                            $addon = bab_getAddonInfosInstance('widgets');
                            $icon = $addon->getImagesPath().'32x32/application-octet-stream.png';
                        }

                        $fileName = $file->pop();
                        if ($type == 'public') {
                            $url = ea_Controller()->Event()->DownloadPublicFile($fileName, $event->id)->url();
                        } else {
                            $url = ea_Controller()->Event()->DownloadFile($fileName, $event->id)->url();
                        }
                        $ovElement = array(
                            'EventFileName' => substr(quoted_printable_decode($fileName), 6),
                            'EventFileThumbnail' => $icon,
                            'EventFileAccess' => (int)$access,
                            'EventFileType' => $type,
                            'EventFileDownloadUrl' => $url
                        );
                        $ovContainer[] = $ovElement;
                    }
                }
            }
            break;

        case 'getActivities':
            $ovContainer[] = array(
                'ActivityName' => 'events',
                'ActivityLabel' => ea_translate('Events'),
                'ActivityUrl' => ea_Controller()->Event()->displayViewList()->url() . '&clearcrumbs=1'
            );
            $ovContainer[] = array(
                'ActivityName' => 'contacts',
                'ActivityLabel' => ea_translate('Contacts'),
                'ActivityUrl' => ea_Controller()->Contact()->displayList()->url() . '&clearcrumbs=1'
            );

            if (ea_Access()->administer()) {
                $ovContainer[] = array(
                    'ActivityName' => 'admin',
                    'ActivityLabel' => ea_translate('Administration'),
                    'ActivityUrl' => ea_Controller()->Admin()->displayList()->url() . '&clearcrumbs=1'
                );
            }
            break;



        default:
            break;
    }

    return $ovContainer;
}


