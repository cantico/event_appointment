CREATE TABLE `ea_access_groups` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_object` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `id_group` int(11) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`),
  KEY `id_object` (`id_object`),
  KEY `id_group` (`id_group`)
);

	
