; <?php/*

[general]
name				="event_appointment"
version				="0.10.0"
addon_type			="EXTENSION"
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
description			="Event appointment"
delete				=1
longdesc			="Event appointment"
db_prefix			="ea_"
ov_version			="8.2.0"
php_version			="5.2.0"
mysql_version		="5.0"
author				="cantico"
icon				="icon.png"

[addons]
widgets				="1.0.61"
LibOrm				="0.9.0"
LibFileManagement 	="0.2.37"
LibGeoNames			="0.0.6"
LibTranslate		=">=1.12.0rc3.01"

[functionalities]
jquery				="Available"
Thumbnailer			="Available"

;*/ ?>
