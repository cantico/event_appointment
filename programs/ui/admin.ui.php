<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_BabTableModelView');



function ea_AdminHome()
{
    $W = bab_Widgets();

    $returnFrame = $W->Frame(null, $W->HBoxLayout())->addClass('ea-shopadmin-home');

    $main = $W->Frame(null, $W->VBoxLayout());
    $returnFrame->addItem($main);

    $iconPanel = $W->Frame(null, $W->VBoxLayout())->addClass('ea-shopadmin-icon-panel');
    $main->addItem($iconPanel);
    $iconPanel->addItem($W->Title(ea_translate('Managing'), 3))
    ->addItem(
        $W->FlowLayout()
        ->setHorizontalSpacing(0.5, 'em')
        ->addClass(Func_Icons::ICON_TOP_48)
        ->addItem(
            $link = $W->Link(
                $W->Icon(ea_translate('Rights'), Func_Icons::ACTIONS_SET_ACCESS_RIGHTS),
                ea_Controller()->Admin()->rights()
            )
            ->setTitle(ea_translate('Manage administration access'))
        )
        ->addItem(
            $W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
            ->addItem($W->Label($link->getTitle()))
        )
        ->addItem(
            $link = $W->Link(
                $W->Icon(ea_translate('Category'), Func_Icons::APPS_MULTIMEDIA),
                ea_Controller()->Admin()->categoryList()
            )
            ->setTitle(ea_translate('Define categories and rights assocaited'))
        )
        ->addItem(
            $W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
            ->addItem($W->Label($link->getTitle()))
        )
        ->addItem(
            $link = $W->Link(
                $W->Icon(ea_translate('Type'), Func_Icons::APPS_MULTIMEDIA),
                ea_Controller()->Admin()->typeList()
            )
            ->setTitle(ea_translate('Define event types'))
        )
        ->addItem(
            $W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
            ->addItem($W->Label($link->getTitle()))
        )
        ->addItem(
            $link = $W->Link(
                $W->Icon(ea_translate('Events'), Func_Icons::ACTIONS_VIEW_PIM_CALENDAR),
                ea_Controller()->Event()->displayList()
            )->setSizePolicy(Func_Icons::ICON_TOP_48)
            ->setTitle(ea_translate('Manage events'))
        )
        ->addItem(
            $W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
            ->addItem($W->Label($link->getTitle()))
        )
        ->addItem(
            $link = $W->Link(
                $W->Icon(ea_translate('Subscription'), Func_Icons::APPS_GROUPS),
                ea_Controller()->Contact()->displayList()
            )->setSizePolicy(Func_Icons::ICON_TOP_48)
            ->setTitle(ea_translate('Manage events subscriptions'))
        )
        ->addItem(
            $W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
            ->addItem($W->Label($link->getTitle()))
        )

    );

    return $returnFrame;
}





class ea_AdminEditor extends Widget_Form
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        if (null === $layout)
        {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
        }

        parent::__construct($id, $layout);

        $this->setName('options');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->addItem(
            $W->SubmitButton()
                ->setAction(ea_Controller()->Admin()->saveRights())
                ->setSuccessAction(ea_Controller()->Admin()->home())
                ->setFailedAction(ea_Controller()->Admin()->rights())
                ->setLabel(ea_translate('Save'))
        );

        $this->loadValues();
    }


    protected function loadValues()
    {
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';
        $this->setValue(array('options', 'access'), aclGetRightsString('ea_access_groups', 1));
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem($W->Acl()->setName('access')->setTitle(ea_translate('Who are the event mangers?')));
    }



}



class ea_CategoryRightsEditor extends Widget_Form
{
    private $category = false;

    public function __construct($category, $id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        $this->category = $category;

        if (null === $layout)
        {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
        }

        parent::__construct($id, $layout);

        $this->setName('options');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setHiddenValue('category', $category);

        $this->addFields();

        if(bab_isAjaxRequest()) {
            $this->addItem(
                $W->SubmitButton()
                    ->setAjaxAction(ea_Controller()->Admin()->saveCategoryRights())
                    ->setLabel(ea_translate('Save'))
            );
        }else{
            $this->addItem(
                $W->SubmitButton()
                    ->setAction(ea_Controller()->Admin()->saveCategoryRights())
                    ->setSuccessAction(ea_Controller()->Admin()->categoryList())
                    ->setFailedAction(ea_Controller()->Admin()->editCategoryRights())
                    ->setLabel(ea_translate('Save'))
            );
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';
        $this->setValue(array('options', 'access'), aclGetRightsString('ea_eventaccess_groups', $this->category));
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem($W->Acl()->setName('access')->setTitle(ea_translate('Who can sign up to this cetegory of events?')));
    }



}




/**
 * Liste des types d'encombrants
 */
class ea_CategoryList extends widget_BabTableModelView
{

    public function addDefaultColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->name, ea_translate('Category')));
        $this->addColumn(widget_TableModelViewColumn($set->color, ea_translate('Color')));

        return $this;
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {

        $controller = ea_Controller()->Admin();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'color':
                return $W->Html('<div style="width:100%; height:100%; background-color:#'.$record->color.';">&nbsp;</div>');
                break;
            case '_edit_':
                return $W->HboxItems(
                    $W->Link(
                        $W->Icon('', Func_Icons::STATUS_DIALOG_PASSWORD),
                        $controller->editCategoryRights($record->id)
                    )
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle(ea_translate('Set access right for this category')),
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $controller->editCategory($record->id)
                    )
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle(ea_translate('Edit')),
                    $W->Link(
                        $temp = $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                        $controller->deleteCategory($record->id)
                    )->setTitle(ea_translate('Delete'))
                    ->setAjaxAction($controller->deleteCategory($record->id), $temp)
                    ->setConfirmationMessage(ea_translate('This will remove this category and all linked event will be unlink, proceed?'))
                );
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

}


class ea_CategoryEditor extends Widget_Form
{
    public function __construct($category = null)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('category');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->category = $category;

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(ea_Controller()->Admin()->saveCategory())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(ea_Controller()->Admin()->saveCategory())
                        ->setSuccessAction(ea_Controller()->Admin()->categoryList())
                        ->setFailedAction(ea_Controller()->Admin()->editCategory())
                        ->setLabel(ea_translate('Save'))
                )
            );
        }

        if($this->category && !bab_isAjaxRequest()){
            $buttonLayout->addItem(
                $W->Link(
                    $W->Icon(ea_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    ea_Controller()->Admin()->deleteCategory($category)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        $this->setHiddenValue('category[id]', $this->category);
        if($this->category){
            $set = new ea_CategorySet();
            $category = $set->get($this->category);
            if($category){
                $this->setValues($category->getValues(), array('category'));
            }
        }
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->LabelledWidget(
                ea_translate('Name'),
                $W->LineEdit()->setMandatory(true, ea_translate('The category name can not be empty')),
                'name'
            )->colon(true)
        );
        $this->addItem(
            $W->LabelledWidget(
                ea_translate('Color'),
                $W->ColorPicker()->setMandatory(true, ea_translate('The color name can not be empty')),
                'color'
            )->colon(true)
        );
    }
}




/**
 * Liste des types d'encombrants
 */
class ea_TypeList extends widget_BabTableModelView
{

    public function addDefaultColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->name, ea_translate('Type')));

        return $this;
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {

        $controller = ea_Controller()->Admin();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case '_edit_':
                return $W->HboxItems(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $controller->editType($record->id)
                    )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle(ea_translate('Edit')),
                    $W->Link(
                        $temp = $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                        $controller->deleteType($record->id)
                    )->setTitle(ea_translate('Delete'))
                    ->setAjaxAction($controller->deleteType($record->id), $temp)
                    ->setConfirmationMessage(ea_translate('This will remove this type and all linked event will be unlink, proceed?'))
                );
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

}


class ea_TypeEditor extends Widget_Form
{
    public function __construct($type = null)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('type');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->type = $type;

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(ea_Controller()->Admin()->saveType())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(ea_Controller()->Admin()->saveType())
                        ->setSuccessAction(ea_Controller()->Admin()->typeList())
                        ->setFailedAction(ea_Controller()->Admin()->editType())
                        ->setLabel(ea_translate('Save'))
                )
            );
        }

        if($this->type && !bab_isAjaxRequest()){
            $buttonLayout->addItem(
                $W->Link(
                    $W->Icon(ea_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    ea_Controller()->Admin()->deleteType($type)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        $this->setHiddenValue('type[id]', $this->type);
        if($this->type){
            $set = new ea_TypeSet();
            $type = $set->get($this->type);
            if($type){
                $this->setValues($type->getValues(), array('type'));
            }
        }
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->LabelledWidget(
                ea_translate('Name'),
                $W->LineEdit()->setMandatory(true, ea_translate('The type name can not be empty')),
                'name'
            )->colon(true)
        );
    }
}