<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";
require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_BabTableModelView');




/**
 * Liste des types d'encombrants
 */
class ea_ContactList extends widget_BabTableModelView
{

    public function addDefaultColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setExportable(false)->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->lastname, ea_translate('Lastname'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->firstname, ea_translate('Firstname')));
        $this->addColumn(widget_TableModelViewColumn($set->event->name, ea_translate('Event name')));
        $this->addColumn(widget_TableModelViewColumn($set->event->start, ea_translate('Start date')));
        $this->addColumn(widget_TableModelViewColumn($set->event->end, ea_translate('End date'))->setVisible(false)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->event->category->name, ea_translate('Category'))->setVisible(false));

        return $this;
    }

    public function addSpecifEventColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setExportable(false)->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->lastname, ea_translate('Lastname'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->firstname, ea_translate('Firstname')));
        $this->addColumn(widget_TableModelViewColumn($set->event->name, ea_translate('Event name'))->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->event->id, ea_translate('Event'))->setExportable(false)->setSortable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($set->event->start, ea_translate('Start date'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->event->end, ea_translate('End date'))->setVisible(false)->setSearchable(false)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->event->category->name, ea_translate('Category'))->setVisible(false)->setSearchable(false));

        return $this;
    }

    protected function handleFilterInputWidget($fieldName, $field)
    {
        $W = bab_Widgets();

        switch($fieldName){
            case 'event/category/name':
                $categories = array(''=>'');
                $categorySet = ea_CategorySet();
                $categoryResult = $categorySet->select();
                foreach($categoryResult as $category){
                    $categories[$category->id] = $category->name;
                }
                return $W->Select()->setOptions($categories)->setName($fieldName);
                break;
            case 'event/id':
				$filter = $this->getFilterValues();
				$set = ea_EventSet();
				$event = $set->get($set->id->is($filter[$fieldName]));
                $events = array(
                	$event->id => $event->name,
                	'' => ea_translate('All events')
				);
				
                return $W->Select()->setOptions($events)->setName($fieldName);
                break;
            default:
                return parent::handleFilterInputWidget($fieldName, $field);
                break;
        }
    }

    protected function handleFilterLabelWidget($fieldName, $field)
    {
        if($fieldName == 'event/start')
        {
            return new Widget_Label(ea_translate('Event periode'));
        }
        if($fieldName == 'firstname')
        {
            return new Widget_Label(ea_translate('Sign up user'));
        }
        return parent::handleFilterLabelWidget($fieldName, $field);
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {

        $controller = ea_Controller()->Contact();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case '_edit_':
                $computed = $W->HboxItems(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $controller->editContact($record->id)
                    )
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle(ea_translate('Edit')),
                    $W->Link(
                        $temp = $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                        $controller->deleteContact($record->id)
                    )->setTitle(ea_translate('Delete'))
                    ->setAjaxAction($controller->deleteContact($record->id), $temp)
                    ->setConfirmationMessage(ea_translate('This will remove this sign up user, proceed?'))
                );
				
				if ($record->description) {
					$computed->addItem(
						$W->Icon('', Func_Icons::OBJECTS_PUBLICATION_TOPIC)->setTitle(ea_translate('There is a comment on this inscription.'))
					);
				}
				
				return $computed;
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

}


class ea_ContactEditor extends Widget_Form
{
    public function __construct($contact = null, $event = null, $fromcalendar = false)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->event = $event;
        $this->contact = $contact;
        $this->fromcalendar = $fromcalendar;
        $this->setName('contact');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();
        $this->colon(true);

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(ea_Controller()->Contact()->saveContact())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(ea_Controller()->Contact()->saveContact())
                        ->setSuccessAction(ea_Controller()->Contact()->signupSuccessfull($this->fromcalendar))
                        ->setFailedAction(ea_Controller()->Contact()->signup($this->event))
                        ->setLabel(ea_translate('Sign up me'))
                )
            );
        }

        if($this->contact && !bab_isAjaxRequest()){
            $buttonLayout->addItem(
                $W->Link(
                    $W->Icon(ea_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    ea_Controller()->Contact()->deleteContact($this->contact)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        $this->setHiddenValue('contact[id]', $this->contact);
        if($this->contact){
            $set = new ea_ContactSet();
            $contact = $set->get($this->contact);
            if($contact){
                $this->setValues($contact->getValues(), array('contact'));
            }
        }
    }

    protected function lastnameField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Lastname'),
            $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, ea_translate('The Lastname can not be empty')),
            'lastname'
        );
    }

    protected function firstnameField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Firstname'),
            $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, ea_translate('The Firstname can not be empty')),
            'firstname'
        );
    }

    protected function organizationField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Organization'),
            $W->LineEdit()->addClass('widget-100pc'),
            'organization'
        );
    }

    protected function emailField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Email'),
            $W->EmailLineEdit()->addClass('widget-100pc')->setMandatory(true, ea_translate('The email can not be empty')),
            'email'
        );
    }

    protected function phoneField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Phone'),
            $W->TelLineEdit()->addClass('widget-100pc')->setMandatory(true, ea_translate('The phone can not be empty')),
            'phone'
        );
    }

    protected function descriptionField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Comment'),
            $W->TextEdit()->setLines(5)->addClass('widget-100pc'),
            'description'
        );
    }

    protected function eventField()
    {
        $W = bab_Widgets();

        $eventOptions = array('', '');
        $eventSet = ea_EventSet();
        $events = $eventSet->select();
        foreach($events as $event){
            $eventOptions[$event->id] = $event->name;
        }
        if($this->event && isset($eventOptions[$this->event])){
            return $W->FlowItems(
                $W->LabelledWidget(
                    ea_translate('Event'),
                    $W->Select()->setDisplayMode()->setOptions($eventOptions)->setValue($this->event),
                    'event'
                ),
                $W->Hidden()->setName('event')->setValue($this->event)
            );
        }else{
            return $W->LabelledWidget(
                ea_translate('Event'),
                $W->Select()->setMandatory(true, ea_translate('The sign up user has to be attach to an event'))->setOptions($eventOptions),
                'event'
            );
        }
    }

    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $this->eventField()
        );
        $this->addItem(
            $this->lastnameField()
        );
        $this->addItem(
            $this->firstnameField()
        );
        $this->addItem(
            $this->organizationField()
        );
        $this->addItem(
            $this->emailField()
        );
        $this->addItem(
            $this->phoneField()
        );
        $this->addItem(
            $this->descriptionField()
        );
    }
}