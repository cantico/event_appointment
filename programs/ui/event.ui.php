<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";
require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_BabTableModelView');




/**
 * Liste des types d'encombrants
 */
class ea_EventList extends widget_BabTableModelView
{

    public function addDefaultColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->name, ea_translate('Name')));
        $this->addColumn(widget_TableModelViewColumn('_subscriber_', ea_translate('Subscribers')));
        $this->addColumn(widget_TableModelViewColumn($set->start, ea_translate('Start date')));
        $this->addColumn(widget_TableModelViewColumn($set->end, ea_translate('End date'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->category->name, ea_translate('Category')));
        $this->addColumn(widget_TableModelViewColumn($set->type->name, ea_translate('Type')));
        $this->addColumn(widget_TableModelViewColumn($set->address->city, ea_translate('City')));
        $this->addColumn(widget_TableModelViewColumn($set->author, ea_translate('Author')));

        return $this;
    }

    public function addDefaultViewColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_editview_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->name, ea_translate('Name')));
        $this->addColumn(widget_TableModelViewColumn($set->start, ea_translate('Start date')));
        $this->addColumn(widget_TableModelViewColumn($set->end, ea_translate('End date'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->category->name, ea_translate('Category')));
        $this->addColumn(widget_TableModelViewColumn($set->type->name, ea_translate('Type')));
        $this->addColumn(widget_TableModelViewColumn($set->address->city, ea_translate('City')));

        return $this;
    }


    protected function handleFilterInputWidget($fieldName, $field)
    {
        $W = bab_Widgets();

        switch($fieldName){
            case 'address/city':
                return $W->SuggestPlaceName()->setName($fieldName);
                break;
            case 'category/name':
                $categories = array(''=>'');
                $categorySet = ea_CategorySet();
                $categoryResult = $categorySet->select();
                foreach($categoryResult as $category){
                    $categories[$category->id] = $category->name;
                }
                return $W->Select()->setOptions($categories)->setName($fieldName);
                break;
            case 'type/name':
                $types = array(''=>'');
                $typeSet = ea_TypeSet();
                $typeResult = $typeSet->select();
                foreach($typeResult as $type){
                    $types[$type->id] = $type->name;
                }
                return $W->Select()->setOptions($types)->setName($fieldName);
                break;
            default:
                return parent::handleFilterInputWidget($fieldName, $field);
                break;
        }
    }

    protected function handleFilterLabelWidget($fieldName, $field)
    {
        if($fieldName == 'start')
        {
            return new Widget_Label(ea_translate('Event periode'));
        }
        return parent::handleFilterLabelWidget($fieldName, $field);
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {

        $controller = ea_Controller()->Event();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'author':
                return $W->Label(bab_getUserName($record->author));
            case '_edit_':
                $computed = $W->HboxItems(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $controller->editEvent($record->id)
                    )
                    //->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle(ea_translate('Edit')),
                    $W->Link(
                        $W->Icon('', Func_Icons::MIMETYPES_TEXT_HTML),
                        $controller->viewMapEvent($record->id)
                    )
                    ->setOpenMode(Widget_Link::OPEN_DIALOG)
                    ->setTitle(ea_translate('View google map')),
                    $W->Link(
                        $temp = $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                        $controller->deleteEvent($record->id)
                    )->setTitle(ea_translate('Delete'))
                    ->setAjaxAction()
                    ->setConfirmationMessage(ea_translate('This will remove this event and all linked sign up user will be deleted, proceed?'))
                );

                return $computed;
                break;
            case '_subscriber_':
                    $set = ea_ContactSet();
                    $contacts = $set->select($set->event->is($record->id));
                    $count = count($contacts);
                    return $W->Link(
                        $W->Icon('('.$count.')', Func_Icons::APPS_GROUPS),
                        ea_Controller()->Contact()->displayList(array('filter' => array('event/id' => $record->id)))
                    )->setTitle(ea_translate('View subscriber list'));
            case '_editview_':
                return $W->HboxItems(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_GO_NEXT),
                        $controller->eventView($record->id)
                    )
                    ->setTitle(ea_translate('View complete event information'))
                );
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

}


class ea_EventEditor extends Widget_Form
{
    public function __construct($event = null)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->event = $event;
        $this->setName('event');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();
        $this->colon(true);

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(ea_Controller()->Event()->saveEvent())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(ea_Controller()->Event()->saveEvent())
                        ->setSuccessAction(ea_Controller()->Event()->displayList())
                        ->setFailedAction(ea_Controller()->Event()->editEvent())
                        ->setLabel(ea_translate('Save'))
                )
            );
        }

        $this->buttonLayout = $buttonLayout;

        $this->addActionLink();

        $this->loadValues();
    }


    protected function addActionLink(){
        $W = bab_Widgets();
        if($this->event && !bab_isAjaxRequest()){
            $this->buttonLayout->addItem(
                $W->Link(
                    $W->Icon(ea_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    ea_Controller()->Event()->deleteEvent($this->event)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }
    }


    protected function loadValues()
    {
        $this->setHiddenValue('event[id]', $this->event);
        if($this->event){
            $set = new ea_EventSet();
            $set->address();
            $event = $set->get($this->event);
            if($event){
                $this->setValues($event->getValues(), array('event'));
            }
        }
    }

    protected function nameField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Name'),
            $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, ea_translate('The event name can not be empty')),
            'name'
        );
    }

    protected function descriptionField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Description'),
            $W->BabHtmlEdit(),
            /*$W->TextEdit()->setLines(5)->addClass('widget-100pc'),*/
            'description'
        );
    }

    protected function startField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Start date'),
            $W->DateTimePicker()->setMandatory(true, ea_translate('The start date is mandatory')),
            'start'
        );
    }

    protected function endField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('End date'),
            $W->DateTimePicker()->setMandatory(true, ea_translate('The end date is mandatory')),
            'end'
        );
    }

    protected function addressField()
    {
        $countryOptions = array('','');
        $countrySet = ea_CountrySet();
        $countries = $countrySet->select();
        foreach($countries as $country){
            if($country->getName() == 'France')
            {
                $valueCountry = $country->id;
            }
            $countryOptions[$country->id] = $country->getName();
        }

        $W = bab_Widgets();
        $return = $W->NamedContainer('address')->addItem(
            $W->VBoxItems(
                $W->Title(ea_translate('Address'), 4),
                $W->LabelledWidget(
                    ea_translate('Location'),
                    $W->LineEdit()->addClass('widget-90pc'),
                    'location'
                ),
                $W->FlowItems(
                    $W->LabelledWidget(
                        ea_translate('Street'),
                        $W->LineEdit()->addClass('widget-100pc'),
                        'street'
                    )->setSizePolicy('widget-70pc'),
                    $W->LabelledWidget(
                        ea_translate('Complement'),
                        $W->LineEdit()->addClass('widget-100pc'),
                        'cityComplement'
                    )->setSizePolicy('widget-20pc')
                )->setHorizontalSpacing(1,'em'),
                $W->FlowItems(
                    $W->LabelledWidget(
                        ea_translate('Postal code'),
                        $PCsuggest = $W->SuggestPostalCode(),
                        'postalCode'
                    ),
                    $W->LabelledWidget(
                        ea_translate('City'),
                        $Csuggest = $W->SuggestPlaceName(),
                        'city'
                    ),
                    $W->LabelledWidget(
                        ea_translate('Country'),
                        $W->select()->setOptions($countryOptions)->setValue($valueCountry),
                        'country'
                    )
                )->setHorizontalSpacing(1,'em'),
                $W->Section(
                    ea_translate('GPS coordinates (Optionnal, only if the current map location is incorrect)'),
                    $W->FlowItems(
                        $W->LabelledWidget(
                            ea_translate('Latitude'),
                            $W->LineEdit(),
                            'latitude'
                        ),
                        $W->LabelledWidget(
                            ea_translate('Longitude'),
                            $W->LineEdit(),
                            'longitude'
                        )
                    )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em'),
                    4
                )->setFoldable(true, true)
            )->setVerticalSpacing(1,'em')
        );

        $Csuggest->setRelatedPostalCode($PCsuggest);
        $PCsuggest->setRelatedPlaceName($Csuggest);

        return $return;
    }

    protected function categoryField()
    {
        $categoryOptions = array('', '');
        $categorySet = ea_CategorySet();
        $categories = $categorySet->select();
        foreach($categories as $category){
            $categoryOptions[$category->id] = $category->name;
        }

        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Category'),
            $W->Select()->setOptions($categoryOptions),
            'category'
        );
    }

    protected function typeField()
    {
        $typeOptions = array('', '');
        $typeSet = ea_TypeSet();
        $types = $typeSet->select();
        foreach($types as $type){
            $typeOptions[$type->id] = $type->name;
        }

        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Type'),
            $W->Select()->setOptions($typeOptions),
            'type'
        );
    }

    protected function numSlotField()
    {
        $W = bab_Widgets();
        return $W->FlowItems(
            $W->LabelledWidget(
                ea_translate('Number of slot maximum'),
                $W->LineEdit(),
                'max_slot'
            ),
            $W->LabelledWidget(
                ea_translate('Number of slot minimum'),
                $W->LineEdit(),
                'min_slot'
            )
        )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em');
    }

    protected function signupField()
    {
        $W = bab_Widgets();

        $signUp = $W->LabelledWidget(
            ea_translate('Sign up'),
            $signUpSelect = $W->Select()->setOptions(
                array(
                    ''=>'',
                    '0'=> ea_translate('No'),
                    '1'=> ea_translate('Yes')
                )
            ),
            'signup'
        );
        $signUpLimit = $W->LabelledWidget(
            ea_translate('Limit date to sign up'),
            $W->DatePicker(),
            'limit_signup'
        );
        $signUpSelect->setAssociatedDisplayable($signUpLimit, array('1'));

        return $W->FlowItems(
            $signUp,
            $signUpLimit
        )->setHorizontalSpacing(1,'em');
    }

    protected function priceField()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            ea_translate('Price'),
            $W->LineEdit(),
            'price'
        );
    }

    protected function contactField()
    {
        $W = bab_Widgets();
        return $W->FlowItems(
            $W->LabelledWidget(
                ea_translate('Internal contact'),
                $W->UserPicker(),
                'internal_contact'
            ),
            $W->LabelledWidget(
                ea_translate('External contact'),
                $W->LineEdit()->addClass('widget-100pc'),
                'external_contact'
            )->setSizePolicy('widget-50pc')
        )->setVerticalAlign("middle")->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em');
    }

    protected function privateFileField()
    {
        $path = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        if ($this->event) {
            $path->push('event');
            $path->push($this->event);
        } else {
            $path->push('tmp');
            $path->push(session_id());
            $path->createDir();
        }

        $W = bab_Widgets();
        return $W->Section(
            ea_translate('Private documents'),
            $W->FilePicker()->setFolder($path)->setTitle(ea_translate('Joined files'))
        );
    }

    protected function publicFileField()
    {
        $path = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        if ($this->event) {
            $path->push('event');
            $path->push($this->event);
            $path->push('public');
        } else {
            $path->push('tmp');
            $path->push(session_id());
            $path->push('public');
            $path->createDir();
        }

        $W = bab_Widgets();

        return $W->Section(
            ea_translate('Public documents'),
            $W->FilePicker()->setFolder($path)->setTitle(ea_translate('Joined files'))
        );
    }

    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $this->nameField()
        );
        $this->addItem(
            $this->descriptionField()
        );
        $this->addItem(
            $W->FlowItems(
                $this->startField(),
                $this->endField()
            )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );
        $this->addItem(
            $this->addressField()
        );
        $this->addItem(
            $W->FlowItems(
                $this->categoryField(),
                $this->typeField()
            )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );
        $this->addItem(
            $this->numSlotField()
        );
        $this->addItem(
            $W->FlowItems(
                $this->signupField(),
                $this->priceField()
            )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );
        $this->addItem(
            $this->contactField()
        );
        $this->addItem(
            $this->privateFileField()
        );
        $this->addItem(
            $this->publicFileField()
        );
    }
}




class ea_EventView extends ea_EventEditor
{
    public function __construct($event, $fromcalendar = false)
    {
        $this->fromcalendar = $fromcalendar;
        parent::__construct($event);
        $this->setDisplayMode();
    }


    protected function addActionLink(){
        $W = bab_Widgets();
        if($this->event && !bab_isAjaxRequest()){
            $already = false;

            if (bab_getUserId()) {
                $set = new ea_ContactSet();
                $userInfo = bab_getUserInfos(bab_getUserId());
                $currentUser = array(
                    'lastname' => $userInfo['sn'],
                    'firstname' => $userInfo['givenname'],
                    'organization' => $userInfo['organisationname'],
                    'email' => $userInfo['email'],
                    'phone' => $userInfo['btel']?$userInfo['btel']:$userInfo['mobile']
                );
                $already = $set->get(
                    $set->event->is($this->event)
                    ->_AND_($set->lastname->is($currentUser['lastname']))
                    ->_AND_($set->firstname->is($currentUser['firstname']))
                    ->_AND_($set->email->is($currentUser['email']))
                );
            }

            if($already) {
                $this->buttonLayout->addItem(
                    $W->Link(
                        $W->Icon(ea_translate('Sign up an other contact'), Func_Icons::ACTIONS_USER_NEW),
                        ea_Controller()->Contact()->signup($this->event, $this->fromcalendar)
                    )->addClass(Func_Icons::ICON_LEFT_16)
                )->setHorizontalSpacing(1, 'em');

                $this->buttonLayout->addItem(
                    $W->Link(
                        $W->Icon(ea_translate('Unsigned'), Func_Icons::ACTIONS_LIST_REMOVE_USER),
                        ea_Controller()->Contact()->unsigned($this->event, $this->fromcalendar)
                    )->addClass(Func_Icons::ICON_LEFT_16)
                    ->setConfirmationMessage(ea_translate('This will remove your participation to this event.'))
                )->setHorizontalSpacing(1, 'em');
            } else {
                $this->buttonLayout->addItem(
                    $W->Link(
                        $W->Icon(ea_translate('Sign up'), Func_Icons::ACTIONS_USER_NEW),
                        ea_Controller()->Contact()->signup($this->event, $this->fromcalendar)
                    )->addClass(Func_Icons::ICON_LEFT_16)
                )->setHorizontalSpacing(1, 'em');
            }
        }
    }

    protected function addressField()
    {
        $countryOptions = array('','');
        $countrySet = ea_CountrySet();
        $countries = $countrySet->select();
        foreach($countries as $country){
            if($country->getName() == 'France')
            {
                $valueCountry = $country->id;
            }
            $countryOptions[$country->id] = $country->getName();
        }

        $W = bab_Widgets();
        $return = $W->NamedContainer('address')->addItem(
            $W->VBoxItems(
                $W->Title(ea_translate('Address'), 4),
                $W->FlowItems(
                    $W->LabelledWidget(
                        ea_translate('Street'),
                        $W->LineEdit()->addClass('widget-100pc'),
                        'street'
                    ),
                    $W->LabelledWidget(
                        ea_translate('Complement'),
                        $W->LineEdit()->addClass('widget-100pc'),
                        'cityComplement'
                    )
                )->setVerticalSpacing(2,'em')->setHorizontalSpacing(2,'em'),
                $W->FlowItems(
                    $W->LabelledWidget(
                        ea_translate('Postal code'),
                        $PCsuggest = $W->SuggestPostalCode(),
                        'postalCode'
                    ),
                    $W->LabelledWidget(
                        ea_translate('City'),
                        $Csuggest = $W->SuggestPlaceName(),
                        'city'
                    ),
                    $W->LabelledWidget(
                        ea_translate('Country'),
                        $W->select()->setOptions($countryOptions)->setValue($valueCountry),
                        'country'
                    )
                )->setVerticalSpacing(2,'em')->setHorizontalSpacing(2,'em'),
                $W->Section(
                    ea_translate('Map'),
                    $this->map(),
                    4
                )->setFoldable(true, false)
            )
        );

        $Csuggest->setRelatedPostalCode($PCsuggest);
        $PCsuggest->setRelatedPlaceName($Csuggest);

        return $return;
    }

    protected function map()
    {
        $W = bab_Widgets();
        $set = new ea_EventSet();
        $set->address();
        $set->address->country();
        $event = $set->get($set->id->is($this->event));

        if (!$event->address->latitude || !$event->address->longitude) {
            $gn = @bab_functionality::get('GeoNames');
            $coordinates = $gn->getAddressCoordinates($event->address->city, $event->address->postalCode, $event->address->street, $event->address->country->getName(), false);
            if (isset($coordinates['longitude']) && isset($coordinates['latitude'])) {
                $event->address->longitude = $coordinates['longitude'];
                $event->address->latitude = $coordinates['latitude'];
                $event->save();
            }
        }

        $addresse = $W->Html(
            '<p>'.$event->name . '<br />' .
            $event->address->street  . '<br />' .
            $event->address->postalCode  . '<br />' .
            $event->address->city  . '<br />' .
            $W->Link(
                ea_translate('Path to go'),
                sprintf(
                    "https://maps.google.fr/maps?saddr=%s&daddr=%s&hl=fr&mra=ls&t=m&z=13",
                    urlencode('Ma position'),
                    urlencode($event->address->latitude .' '. $event->address->longitude)
                )
            )->addAttribute('target', '_blank')->display($W->HtmlCanvas())
        );

        $map = $W->Map('event-map')->setHeight(250)->setWidth(400);
        $map->setCenter($event->address->latitude, $event->address->longitude)->setZoom(15);
        $map->addMarker($event->address->latitude, $event->address->longitude, $event->name, $addresse);

        return $map;
    }

    protected function fileField()
    {
        //return parent::fileField();
        /* @var $path bab_Path */
        $path = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        if ($this->event) {
            $path->push('event');
            $path->push($this->event);
        } else {
            $path->push('tmp');
            $path->push(session_id());
            $path->createDir();
        }

        $W = bab_Widgets();
        $FP = $W->FilePicker()->setFolder($path)->setTitle(ea_translate('Joined files'));

        $layout = $W->FlowLayout()->setHorizontalSpacing(1, 'em')->setVerticalSpacing(1, 'em');

        //$_GET['thumbfile'] = '1';

        foreach($path as $file){
            if($file->isFile()){
                $filepath = $file->toString();

                /*@var $T Func_Thumbnailer */
                $T = @bab_functionality::get('Thumbnailer', false);
                if ($T) {
                    $icon = $T->getIcon($filepath, 32, 32);
                    if (is_object($icon)) {
                        $icon = $icon->__toString();
                    }

                    $icon;
                } else {
                    $addon = bab_getAddonInfosInstance('widgets');
                    $icon = $addon->getImagesPath().'32x32/application-octet-stream.png';
                }

                $fileName = $file->pop();

                $layout->addItem(
                    $W->VBoxItems(
                        $W->Link(
                            $W->Image($icon),
                            ea_Controller()->Event()->DownloadFile($fileName, $this->event)
                        ),
                        $W->Link(
                            $W->Label(Widget_FilePicker::decode($fileName)),
                            ea_Controller()->Event()->DownloadFile($fileName, $this->event)
                        )
                    )->addClass('widget-filepicker-file')
                );
            }
        }

        return $layout;
    }

    /*protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $this->nameField()
        );
        $this->addItem(
            $this->descriptionField()
        );
        $this->addItem(
            $W->FlowItems(
                $this->startField(),
                $this->endField()
            )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );
        $this->addItem(
            $this->addressField()
        );
        $this->addItem(
            $W->FlowItems(
                $this->categoryField(),
                $this->typeField()
            )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );
        $this->addItem(
            $this->numSlotField()
        );
        $this->addItem(
            $W->FlowItems(
                $this->signupField(),
                $this->priceField()
            )->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );
        $this->addItem(
            $this->contactField()
        );
        $this->addItem(
            $this->fileField()
        );
    }*/

}