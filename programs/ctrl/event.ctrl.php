<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/event.ui.php';
require_once dirname(__FILE__).'/../set/category.class.php';
require_once dirname(__FILE__).'/../set/type.class.php';
require_once dirname(__FILE__).'/../set/event.class.php';
require_once dirname(__FILE__).'/../set/country.class.php';
require_once dirname(__FILE__).'/../set/address.class.php';
require_once dirname(__FILE__).'/../set/contact.class.php';

/**
 *
 */
class ea_CtrlEvent extends ea_Controller
{

    private function addItemMenu($page)
    {
        bab_requireCredential();
        $page->addItemMenu('home', ea_translate('Administration menu'), ea_Controller()->Admin()->home()->url());
        return $page;
    }


    public function displayList($search = null)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu(__METHOD__, ea_translate('Event list'), ea_Controller()->Event()->displayList($search)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->events($search);

        $page->addItem($Layout);

        return $page;
    }

    public function getFilterSet($filter, $set)
    {
        bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $criteria = new ORM_TrueCriterion();

        if (!empty($filter['name']))
        {
            $criteria = $criteria->_AND_($set->name->contains($filter['name']));
        }
        if (!empty($filter['start']) && !empty($filter['start']['from']))
        {
            $criteria = $criteria->_AND_($set->end->greaterThanOrEqual(ea_date($filter['start']['from'])));
        }
        if (!empty($filter['start']) && !empty($filter['start']['to']))
        {
            $criteria = $criteria->_AND_($set->end->lessThanOrEqual(ea_date($filter['start']['to'])));
        }
        if (!empty($filter['category/name']))
        {
            $criteria = $criteria->_AND_($set->category->id->is($filter['category/name']));
        }
        if (!empty($filter['type/name']))
        {
            $criteria = $criteria->_AND_($set->type->id->is($filter['type/name']));
        }
        if (!empty($filter['address/city']))
        {
            $criteria = $criteria->_AND_($set->address->city->is($filter['address/city']));
        }
        if (!empty($filter['author']))
        {
            $criteria = $criteria->_AND_($set->author->is($filter['author']));
        }

        return $criteria;
    }

    public function events($search = null)
    {
        bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $VboxLayout->addItem(
            $W->FlowItems(
                $W->Link(
                    ea_translate('Add an event'),
                    ea_Controller()->Event()->editEvent()
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD, 'orgchart-edit-member')
                //->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
        );

        $tableView = new ea_EventList("event-list-tableview");
        $tableView->allowColumnSelection();
        $tableView->setAjaxAction();

        $set = new ea_EventSet();
        $set->category();
        $set->type();
        $set->address();

        $filter = isset($search['filter']) ? $search['filter'] : null;
        if (isset($filter)) {
            $tableView->setFilterValues($filter);
        }
        $filter = $tableView->getFilterValues();

        $criteria = $this->getFilterSet($filter, $set);
        $events = $set->select($criteria);
        $tableView->addClass(Func_Icons::ICON_LEFT_16);
        $tableView->setDataSource($events);
        $tableView->addDefaultColumns($set);

        $searchableList = $tableView->filterPanel('search', $filter)->addClass(Func_Icons::ICON_LEFT_16);

        $VboxLayout->addItem($searchableList);

        $VboxLayout->setReloadAction(ea_Controller()->Event()->events($search));

        return $VboxLayout;
    }


    public function editEvent($id = null)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Event()->displayList()->url());
        $page->addItemMenu(__METHOD__, ea_translate('Add an event'), ea_Controller()->Event()->editEvent($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ea_EventEditor($id);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function deleteEvent($id = null)
    {
        bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $set = new ea_ContactSet();
        $set->delete($set->event->is($id));

        $set = new ea_EventSet();
        $eventORM = $set->get($set->id->is($id));
        $set->delete($set->id->is($id));

        $addressSet = ea_AddressSet();
        $addressSet->delete($addressSet->id->is($eventORM->address));

        $path = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        $path->push('event');
        $path->push($id);
        if ($path->isDir()) {
            $path->deleteDir();
        }

        if(bab_isAjaxRequest()) {
            return true;
        }
        ea_Controller()->Event()->displayList()->location();
    }

    public function saveEvent($event = null)
    {
        bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $set = new ea_EventSet();
        $set->address();
        $eventORM = false;

        if($event['name'] == ''){
            throw new bab_SaveException(ea_translate('The event name can not be empty'));
        }

        if(isset($event['id']) && $event['id']){
            $eventORM = $set->get($event['id']);
        }

        if(!$eventORM){
            $eventORM = $set->newRecord();
        }

        $eventORM->setFormInputValues($event);
        $eventORM->address->setFormInputValues($event['address']);
        $eventORM->author = bab_getUserId();
        $eventORM->save();

        if(!isset($event['id']) || !$event['id']){
            $tmppath = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
            $tmppath->push('tmp');
            $tmppath->push(session_id());

            $path = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
            $path->push('event');
            $path->createDir();
            $path->push($eventORM->id);
            if($path->isDir()){
                $path->deleteDir();
            }

            rename($tmppath->tostring(), $path->tostring());
        }

        return true;
    }

    public function viewMapEvent($id)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Event()->displayList()->url());
        $page->addItemMenu(__METHOD__, ea_translate('Map event'), ea_Controller()->Event()->viewMapEvent($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $set = new ea_EventSet();
        $set->address();
        $set->address->country();
        $event = $set->get($set->id->is($id));

        if (!$event->address->latitude || !$event->address->longitude) {
            $gn = @bab_functionality::get('GeoNames');
            $coordinates = $gn->getAddressCoordinates($event->address->city, $event->address->postalCode, $event->address->street, $event->address->country->getName(), false);
            if (isset($coordinates['longitude']) && isset($coordinates['latitude'])) {
                $event->address->longitude = $coordinates['longitude'];
                $event->address->latitude = $coordinates['latitude'];
                $event->save();
            }
        }

        $addresse = $W->Html(
            '<p>'.$event->name . '<br />' .
            $event->address->street  . '<br />' .
            $event->address->postalCode  . '<br />' .
            $event->address->city  . '<br />' .
            $W->Link(
                ea_translate('Path to go'),
                sprintf(
                    "https://maps.google.fr/maps?saddr=%s&daddr=%s&hl=fr&mra=ls&t=m&z=13",
                    urlencode('Ma position'),
                    urlencode($event->address->latitude .' '. $event->address->longitude)
                )
            )->addAttribute('target', '_blank')->display($W->HtmlCanvas())
        );

        $map = $W->Map('event-map')->setHeight(500)->setWidth(800);
        $map->setCenter($event->address->latitude, $event->address->longitude)->setZoom(15);
        $map->addMarker($event->address->latitude, $event->address->longitude, $event->name, $addresse);

        $page->addItem(
            $W->VBoxItems(
                $W->TItle($event->name, 3),
                $map
            )
        );

        return $page;
    }


////////////////////////////VIEW





    public function displayViewList($search = null)
    {
        bab_requireCredential();
        $W = bab_Widgets();
        $page = ea_Page();
        $page->addItemMenu(__METHOD__, ea_translate('Event list'), ea_Controller()->Event()->displayViewList($search)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->eventsView($search);

        $page->addItem($Layout);

        return $page;
    }

    public function getFilterSetView($filter, $set)
    {
        bab_requireCredential();
        $criteria = $set->category->id->in(ea_getAccessibleCategories());

        if (!empty($filter['name']))
        {
            $criteria = $criteria->_AND_($set->name->contains($filter['name']));
        }
        if (!empty($filter['start']) && !empty($filter['start']['from']))
        {
            $criteria = $criteria->_AND_($set->end->greaterThanOrEqual(ea_date($filter['start']['from'])));
        }
        if (!empty($filter['start']) && !empty($filter['start']['to']))
        {
            $criteria = $criteria->_AND_($set->end->lessThanOrEqual(ea_date($filter['start']['to']).' 23:59:59'));
        }
        if (!empty($filter['category/name']))
        {
            $criteria = $criteria->_AND_($set->category->id->is($filter['category/name']));
        }
        if (!empty($filter['type/name']))
        {
            $criteria = $criteria->_AND_($set->type->id->is($filter['type/name']));
        }
        if (!empty($filter['address/city']))
        {
            $criteria = $criteria->_AND_($set->address->city->is($filter['address/city']));
        }

        return $criteria;
    }

    public function eventsView($search = null)
    {
        bab_requireCredential();
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $tableView = new ea_EventList("event-listview-tableview");
        $tableView->allowColumnSelection();
        $tableView->setAjaxAction();

        $set = new ea_EventSet();
        $set->category();
        $set->type();
        $set->address();

        $filter = isset($search['filter']) ? $search['filter'] : null;
        if (isset($filter)) {
            $tableView->setFilterValues($filter);
        }
        $filter = $tableView->getFilterValues();

        $criteria = $this->getFilterSetView($filter, $set);
        $events = $set->select($criteria);
        $tableView->addClass(Func_Icons::ICON_LEFT_16);

        $tableView->setDataSource($events);
        $tableView->addDefaultViewColumns($set);

        $searchableList = $tableView->filterPanel('search', $filter)->addClass(Func_Icons::ICON_LEFT_16);

        $VboxLayout->addItem($searchableList);

        $VboxLayout->setReloadAction(ea_Controller()->Event()->eventsView($search));

        return $VboxLayout;
    }


    public function eventView($id = null, $fromcalendar = false)
    {
        bab_requireCredential();
        if(!ea_getAccessibleEvents($id)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        if($fromcalendar){
            $page->setEmbedded(false);
        }else{
            $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Event()->displayViewList()->url());
            $page->addItemMenu(__METHOD__, ea_translate('Event info'), ea_Controller()->Event()->eventView($id, $fromcalendar)->url());
            $page->setCurrentItemMenu(__METHOD__);
        }

        $editor = new ea_EventView($id, $fromcalendar);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);


        return $page;
    }

    public function DownloadPublicFile($name, $id)
    {

        /* @var $path bab_Path */
        $babpath = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        $babpath->push('event');
        $babpath->push($id);
        $babpath->push('public');
        $babpath->push($name);

        if($babpath->isFile()){
            $W = bab_Widgets();
            $babpath->pop();
            $FP = $W->FilePicker();
            $item = $FP->getByName($babpath, $name);
            $item->download(false);
        }

        throw new Exception(ea_translate('Access denied'));
    }

    public function DownloadFile($name, $id)
    {
        bab_requireCredential();
        if(!ea_getAccessibleEvents($id)){
            throw new Exception(ea_translate('Access denied'));
        }

        /* @var $path bab_Path */
        $babpath = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        $babpath->push('event');
        $babpath->push($id);
        $babpath->push($name);

        if($babpath->isFile()){
            $W = bab_Widgets();
            $babpath->pop();
            $FP = $W->FilePicker();
            $item = $FP->getByName($babpath, $name);
            $item->download(false);
        }

        throw new Exception(ea_translate('Access denied'));
    }

    /*public function moveFile()
    {
        bab_requireCredential();
        $babpath = new bab_Path(bab_getAddonInfosInstance('event_appointment')->getUploadPath());
        $babpath->push('event');

        $set = new ea_EventSet();

        foreach ($babpath as $folder) {
            $id = $folder->pop();
            $event = $set->get($set->id->is($id));
            if ($event && $event->category && $event->category != '3') {
                $folder->push($id);
                foreach ($folder as $file) {
                    if ($file->isFile()) {
                        $src = $file->tostring();
                        $name = $file->pop();
                        $file->push('public');
                        $file->createDir();
                        $file->push($name);
                        rename($src, $file->tostring());
                    }
                }
            }
        }
    }*/
}
