<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/admin.ui.php';
require_once dirname(__FILE__).'/../set/category.class.php';
require_once dirname(__FILE__).'/../set/type.class.php';
require_once dirname(__FILE__).'/../set/event.class.php';
require_once dirname(__FILE__).'/../set/country.class.php';
require_once dirname(__FILE__).'/../set/address.class.php';
require_once dirname(__FILE__).'/../set/contact.class.php';

/**
 *
 */
class ea_CtrlAdmin extends ea_Controller
{

    private function addItemMenu($page)
    {
    	bab_requireCredential();
        $page->addItemMenu('home', ea_translate('Administration menu'), ea_Controller()->Admin()->home()->url());
        return $page;
    }

    public function home()
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->setCurrentItemMenu(__METHOD__);

        $HomeFrame = ea_AdminHome();
        $page->addItem($HomeFrame);

        return $page;
    }

    public function saveRights($options = array())
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';

        aclSetRightsString('ea_access_groups', 1, $options['access']);

        return true;
    }

    public function rights()
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu(__METHOD__, ea_translate('Rights'), ea_Controller()->Admin()->rights()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ea_AdminEditor();
        $page->addItem($editor);

        return $page;
    }

    public function saveCategoryRights($options = array(), $category = null)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';

        aclSetRightsString('ea_eventaccess_groups', $category, $options['access']);

        return true;
    }

    public function editCategoryRights($category)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu('categoryList', ea_translate('Category list'), ea_Controller()->Admin()->categoryList()->url());
        $page->addItemMenu(__METHOD__, ea_translate('Category rights'), ea_Controller()->Admin()->editCategoryRights($category)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ea_CategoryRightsEditor($category);
        if(bab_isAjaxRequest()) {
            return $editor;
        }
        $page->addItem($editor);

        return $page;
    }

    public function categoryList()
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu(__METHOD__, ea_translate('Category list'), ea_Controller()->Admin()->categoryList()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->categories();

        $page->addItem($Layout);

        return $page;
    }

    public function categories()
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $VboxLayout->addItem(
            $W->FlowItems(
                $W->Link(
                    ea_translate('Add a category'),
                    ea_Controller()->Admin()->editCategory()
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD, 'orgchart-edit-member')
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
        );

        $tableView = new ea_CategoryList();
        $set = new ea_CategorySet();
        $types = $set->select();
        $tableView->addClass(Func_Icons::ICON_LEFT_16);
        $tableView->setDataSource($types);
        $tableView->addDefaultColumns($set);

        $VboxLayout->addItem($tableView);

        $VboxLayout->setReloadAction(ea_Controller()->Admin()->categories());

        return $VboxLayout;
    }


    public function editCategory($id = null)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu('categoryList', ea_translate('Category list'), ea_Controller()->Admin()->categoryList()->url());
        $page->addItemMenu(__METHOD__, ea_translate('Add a Category'), ea_Controller()->Admin()->editCategory($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ea_CategoryEditor($id);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function deleteCategory($id = null)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $set = new ea_CategorySet();
        $set->delete($set->id->is($id));

        if(bab_isAjaxRequest()) {
            return true;
        }
        ea_Controller()->Admin()->categoryList()->location();
    }

    public function saveCategory($category = null)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $set = new ea_CategorySet();
        $categoryORM = false;

        if($category['name'] == ''){
            throw new bab_SaveException(ea_translate('The category name can not be empty'));
        }

        if(isset($category['id']) && $category['id']){
            $categoryORM = $set->get($category['id']);
        }

        if(!$categoryORM){
            $categoryORM = $set->newRecord();
        }

        $categoryORM->setFormInputValues($category);
        $categoryORM->save();

        return true;
    }



    public function typeList()
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu(__METHOD__, ea_translate('Type list'), ea_Controller()->Admin()->typeList()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->types();

        $page->addItem($Layout);

        return $page;
    }

    public function types()
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $VboxLayout->addItem(
            $W->FlowItems(
                $W->Link(
                    ea_translate('Add a type'),
                    ea_Controller()->Admin()->editType()
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD, 'orgchart-edit-member')
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
        );

        $tableView = new ea_TypeList();
        $set = new ea_TypeSet();
        $types = $set->select();
        $tableView->addClass(Func_Icons::ICON_LEFT_16);
        $tableView->setDataSource($types);
        $tableView->addDefaultColumns($set);

        $VboxLayout->addItem($tableView);

        $VboxLayout->setReloadAction(ea_Controller()->Admin()->types());

        return $VboxLayout;
    }


    public function editType($id = null)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu('typeList', ea_translate('Type list'), ea_Controller()->Admin()->typeList()->url());
        $page->addItemMenu(__METHOD__, ea_translate('Add a type'), ea_Controller()->Admin()->editType($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ea_TypeEditor($id);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function deleteType($id = null)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $set = new ea_TypeSet();
        $set->delete($set->id->is($id));

        if(bab_isAjaxRequest()) {
            return true;
        }
        ea_Controller()->Admin()->typeList()->location();
    }

    public function saveType($type = null)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new Exception(ea_translate('Access denied'));
        }
        $set = new ea_TypeSet();
        $typeORM = false;

        if($type['name'] == ''){
            throw new bab_SaveException(ea_translate('The type name can not be empty'));
        }

        if(isset($type['id']) && $type['id']){
            $typeORM = $set->get($type['id']);
        }

        if(!$typeORM){
            $typeORM = $set->newRecord();
        }

        $typeORM->setFormInputValues($type);
        $typeORM->save();

        return true;
    }


}