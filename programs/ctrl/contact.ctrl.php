<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/contact.ui.php';
require_once dirname(__FILE__).'/../set/category.class.php';
require_once dirname(__FILE__).'/../set/type.class.php';
require_once dirname(__FILE__).'/../set/event.class.php';
require_once dirname(__FILE__).'/../set/country.class.php';
require_once dirname(__FILE__).'/../set/address.class.php';
require_once dirname(__FILE__).'/../set/contact.class.php';

/**
 *
 */
class ea_CtrlContact extends ea_Controller
{

    private function addItemMenu($page)
    {
    	bab_requireCredential();
        $page->addItemMenu('home', ea_translate('Administration menu'), ea_Controller()->Admin()->home()->url());
        return $page;
    }


    public function displayList($search = null, $csv = false)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu(__METHOD__, ea_translate('Sign up list'), ea_Controller()->Contact()->displayList($search, $csv)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->signups($search, $csv);

        $page->addItem($Layout);

        return $page;
    }

    public function getFilterSet($filter, $set)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
        $criteria = new ORM_TrueCriterion();

        if(!empty($filter['event/id'])){
            $criteria = $criteria->_AND_($set->event->id->is($filter['event/id']));
        }else{
            if (!empty($filter['event/start']) && !empty($filter['event/start']['from']))
            {
                $criteria = $criteria->_AND_($set->event->end->greaterThanOrEqual(ea_date($filter['event/start']['from'])));
            }
            if (!empty($filter['event/start']) && !empty($filter['event/start']['to']))
            {
                $criteria = $criteria->_AND_($set->event->end->lessThanOrEqual(ea_date($filter['event/start']['to']).' 23:59:59'));
            }
            if (!empty($filter['event/category/name']))
            {
                $criteria = $criteria->_AND_($set->event->category->id->is($filter['event/category/name']));
            }
            if (!empty($filter['event/name']))
            {
                $criteria = $criteria->_AND_($set->event->name->contains($filter['event/name']));
            }
        }

        if (!empty($filter['firstname']))
        {
            $criteria = $criteria->_AND_(
                $set->firstname->contains($filter['firstname'])
                ->_OR_($set->lastname->contains($filter['firstname']))
            );
        }

        return $criteria;
    }

    public function signups($search = null, $csv = false)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $VboxLayout->addItem(
            $W->FlowItems(
                $W->Link(
                    ea_translate('Add a sign up user'),
                    ea_Controller()->Contact()->editContact()
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD, 'orgchart-edit-member')
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                $W->Link(
                    ea_translate('Export in XLSX'),
                    ea_Controller()->Contact()->displayList($search, true)
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_VIEW_LIST_TEXT, 'orgchart-edit-member')
            )->setHorizontalSpacing(1,'em')->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
        );

        $tableView = new ea_ContactList("contact-list-tableview");
        $tableView->allowColumnSelection();
        $tableView->setAjaxAction();

        $set = new ea_ContactSet();
        $set->event();
        $set->event->category();

        $filter = isset($search['filter']) ? $search['filter'] : null;
        if (isset($filter)) {
            $tableView->setFilterValues($filter);
        }
        $filter = $tableView->getFilterValues();

        $criteria = $this->getFilterSet($filter, $set);
        $events = $set->select($criteria);
        $tableView->addClass(Func_Icons::ICON_LEFT_16);
        $tableView->setDataSource($events);
		if(!empty($filter['event/id'])){
			$tableView->addSpecifEventColumns($set);
		} else {
        	$tableView->addDefaultColumns($set);
		}

        $searchableList = $tableView->filterPanel('search', $filter);

        $VboxLayout->addItem($searchableList);

        $VboxLayout->setReloadAction(ea_Controller()->Contact()->signups(/*$search*/));

        if($csv){
            $tableView->downloadXlsx('export-'.date('Y-m-d').'.xlsx');
            die;
        }

        return $VboxLayout;
    }


    public function editContact($id = null, $idEvent = null)
    {
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        $this->addItemMenu($page);
        $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Contact()->displayList()->url());
        $page->addItemMenu(__METHOD__, ea_translate('Add an event'), ea_Controller()->Contact()->editContact($id, $idEvent)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ea_ContactEditor($id, $idEvent);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function deleteContact($id = null)
    {
    	bab_requireCredential();
        if(!bab_isAccessValid('ea_access_groups', 1)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
        $set = new ea_ContactSet();
        $set->delete($set->id->is($id));

        if(bab_isAjaxRequest()) {
            return true;
        }
        ea_Controller()->Contact()->displayList()->location();
    }

    public function saveContact($contact = null)
    {
    	bab_requireCredential();
        if(!(bab_isAccessValid('ea_access_groups', 1) || (isset($contact['event']) && ea_getAccessibleEvents($contact['event'])))){
            /*throw new bab_AccessException(ea_translate('Access denied'));*/
            return false;
        }
        $set = new ea_ContactSet();
        $contactORM = false;

        if(isset($contact['id']) && $contact['id']){
            $contactORM = $set->get($contact['id']);
        }
		

        if(!$contactORM){
			$already = $set->get(
				$set->event->is($contact['event'])
				->_AND_($set->lastname->is($contact['lastname']))
				->_AND_($set->firstname->is($contact['firstname']))
				->_AND_($set->email->is($contact['email']))
			);
			
			if($already) {
				throw new bab_SaveException(ea_translate('You are already in'));
				
				return false;
			}
			
            $contactORM = $set->newRecord();
        }

        $contactORM->setFormInputValues($contact);
        $contactORM->save();

        return true;
    }

    ///////////////////////// SIGN UP


    public function signup($idEvent = null, $fromcalendar = false)
    {
    	bab_requireCredential();
        if(!ea_getAccessibleEvents($idEvent)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = ea_Page();
        if($fromcalendar){
            $page->setEmbedded(false);
        }else{
            $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Event()->displayViewList()->url());
            $page->addItemMenu(__METHOD__, ea_translate('Sign up to the event'), ea_Controller()->Contact()->signup($idEvent)->url());
            $page->setCurrentItemMenu(__METHOD__);
        }

        if (bab_getUserId()) {
            $userInfo = bab_getUserInfos(bab_getUserId());
            $currentUser = array(
                'lastname' => $userInfo['sn'],
                'firstname' => $userInfo['givenname'],
                'organization' => $userInfo['organisationname'],
                'email' => $userInfo['email'],
                'phone' => $userInfo['btel']?$userInfo['btel']:$userInfo['mobile']
            );
        } else {
            $currentUser = array();
        }

        $editor = new ea_ContactEditor(null, $idEvent, $fromcalendar);
        $editor->setValues(bab_rp('contact', $currentUser), array('contact'));
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

	public function unsigned($event = null, $fromcalendar = false)
	{
    	bab_requireCredential();
        if(!ea_getAccessibleEvents($idEvent)){
            throw new bab_AccessException(ea_translate('Access denied'));
        }
		
		$W = bab_Widgets();
        $page = ea_Page();
        if($fromcalendar){
            $page->setEmbedded(false);
        }else{
            $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Contact()->displayList()->url());
            $page->addItemMenu(__METHOD__, ea_translate('Sign up to the event'), ea_Controller()->Contact()->signup()->url());
            $page->setCurrentItemMenu(__METHOD__);
        }
		
		$userInfo = bab_getUserInfos(bab_getUserId());
        $currentUser = array(
            'lastname' => $userInfo['sn'],
            'firstname' => $userInfo['givenname'],
            'organization' => $userInfo['organisationname'],
            'email' => $userInfo['email'],
            'phone' => $userInfo['btel']?$userInfo['btel']:$userInfo['mobile']
        );
		
		$set = new ea_ContactSet();
		$set->delete(
			$set->event->is($event)
			->_AND_($set->lastname->is($currentUser['lastname']))
			->_AND_($set->firstname->is($currentUser['firstname']))
			->_AND_($set->email->is($currentUser['email']))
		);

        if($fromcalendar){
            $layout = $W->Layout()->addItem(
                $W->VBoxItems(
                    $W->Label(ea_translate('_unsign_successfull_msg_'))
                )->setVerticalSpacing(1, 'em')->addClass('BabLoginMenuBackground')
                ->addClass('widget-bordered')
            );
        }else{
            $layout = $W->Layout()->addItem(
                $W->VBoxItems(
                    $W->Label(ea_translate('_unsign_successfull_msg_')),
                    $W->Link(ea_translate('Return to the event list'), ea_Controller()->Event()->displayViewList())
                )->setVerticalSpacing(1, 'em')->addClass('BabLoginMenuBackground')
                ->addClass('widget-bordered')
            );
        }

        $page->addItem($layout);

        return $page;
	}

    public function signupSuccessfull($fromcalendar = false)
    {
    	bab_requireCredential();
        $W = bab_Widgets();
        $page = ea_Page();
        if($fromcalendar){
            $page->setEmbedded(false);
        }else{
            $page->addItemMenu('displayList', ea_translate('Event list'), ea_Controller()->Contact()->displayList()->url());
            $page->addItemMenu(__METHOD__, ea_translate('Sign up to the event'), ea_Controller()->Contact()->signup()->url());
            $page->setCurrentItemMenu(__METHOD__);
        }

        if($fromcalendar){
            $layout = $W->Layout()->addItem(
                $W->VBoxItems(
                    $W->Label(ea_translate('_inscription_successfull_msg_'))
                )->setVerticalSpacing(1, 'em')->addClass('BabLoginMenuBackground')
                ->addClass('widget-bordered')
            );
        }else{
            $layout = $W->Layout()->addItem(
                $W->VBoxItems(
                    $W->Label(ea_translate('_inscription_successfull_msg_')),
                    $W->Link(ea_translate('Return to the event list'), ea_Controller()->Event()->displayViewList())
                )->setVerticalSpacing(1, 'em')->addClass('BabLoginMenuBackground')
                ->addClass('widget-bordered')
            );
        }

        $page->addItem($layout);

        return $page;
    }
}
