<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

/**
 *
 */
function event_appointment_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{
    require_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';

    $addon = bab_getAddonInfosInstance('event_appointment');
    bab_functionality::includeOriginal('Icons');
	
	$item = $event->createItem('event_appointment_root');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $item->setLabel(ea_translate('Event appointment'));
    $item->addIconClassname(Func_Icons::ACTIONS_VIEW_PIM_CALENDAR);
    $event->addFunction($item);

    if (bab_isUserAdministrator() || bab_isAccessValid('ea_access_groups', 1))
    {

        $position = array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'event_appointment_root');

        $item = $event->createItem('event_appointment_admin');

        $item->setLabel(ea_translate('Manage events'));
        $item->setDescription(ea_translate('Event appointment management'));
        $item->setLink(ea_Controller()->Admin()->home()->url());
        $item->addIconClassname(Func_Icons::ACTIONS_VIEW_PIM_CALENDAR);
        $item->setPosition($position);

        $event->addFunction($item);

    }

    $position = array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'event_appointment_root');

    $item = $event->createItem('event_appointment_user');
    $item->setLabel(ea_translate('Events list'));
    $item->setDescription(ea_translate('Event appointment'));
    $item->setLink(ea_Controller()->Event()->displayViewList()->url());
    $item->addIconClassname(Func_Icons::ACTIONS_VIEW_PIM_CALENDAR);
    $item->setPosition($position);

    $event->addFunction($item);
}


function event_appointment_onBeforePeriodsCreated(bab_eventBeforePeriodsCreated $event)
{

    $backend = bab_functionality::get('CalendarBackend/eventappointment');

    $periods = $backend->selectPeriods($event->getCriteria());
    foreach ($periods as $period)
    {
        $event->periods->addPeriod($period);
    }

}


function event_appointment_onCollectCalendarsBeforeDisplay(bab_eventCollectCalendarsBeforeDisplay $event)
{
    global $babDB;
    $backend = bab_functionality::get('CalendarBackend/eventappointment');
    /*@var $backend Func_CalendarBackend_eventappointment */

    $categorySet = new ea_CategorySet();
    $categories = $categorySet->select($categorySet->id->in(ea_getAccessibleCategories()));

    foreach($categories as $category)
    {
        $calendar = $backend->PublicCalendar();
        $calendar->init($category->identifier, $category->name, $backend);

        $event->addCalendar($calendar);
    }
}