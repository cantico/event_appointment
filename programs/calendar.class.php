<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';

bab_functionality::includefile('CalendarBackend');

class ea_EventCalendar extends bab_EventCalendar implements bab_PublicCalendar
{
    private $backend = null;
    private $id_table = null;

    public function init($id_table, $name, $backend)
    {
        $this->name = $name;
        $this->id_table = $id_table;
        $this->uid = $id_table;
        $this->backend = $backend;
    }

    public function getBackend()
    {
        return $this->backend;
    }

    public function getType()
    {
        return ea_translate('Event appointment');
    }

    public function getReferenceType()
    {
        return 'ea';
    }


    public function onAddRelation(bab_CalendarPeriod $event)
    {

    }

    public function onUpdateRelation(bab_CalendarPeriod $event)
    {

    }
}