<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));

/**
 *
 * @property ORM_TextField					$street
 * @property ORM_StringField				$postalCode
 * @property ORM_StringField				$city
 * @property ORM_StringField				$cityComplement
 * @property ORM_StringField				$longitude
 * @property ORM_StringField				$latitude
 * @property ea_CountrySet					$country
 *
 */
class ea_AddressSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
        	ORM_TextField('location'),
            ORM_TextField('street'),
            ORM_StringField('postalCode', 10),
            ORM_StringField('city', 60),
            ORM_StringField('cityComplement', 60),
            ORM_StringField('longitude', 60),
            ORM_StringField('latitude', 60)
        );
        $this->hasOne('country', 'ea_CountrySet');
    }
}

/**
 *
 * @property ORM_TextField					$street
 * @property ORM_StringField				$postalCode
 * @property ORM_StringField				$city
 * @property ORM_StringField				$cityComplement
 * @property ORM_StringField				$longitude
 * @property ORM_StringField				$latitude
 * @property ea_CountrySet					$country
 *
 */
class ea_Address extends ORM_MySqlRecord
{

    /**
     * Update properties with CSV row
     *
     * @return 	Array		updated properties from organization record
     */
    public function import(Widget_CsvRow $row)
    {
        $up_prop = 0;


        if ($this->street !== $row->street) {
            $up_prop++;
            $this->street = $row->street;
        }

        if ($this->postalCode !== $row->postalCode) {
            $up_prop++;
            $this->postalCode = $row->postalCode;
        }

        if ($this->city !== $row->city) {
            $up_prop++;
            $this->city = $row->city;
        }

        if ($this->state !== $row->state) {
            $up_prop++;
            $this->state = $row->state;
        }


        $CSet = ea_Controller();
        $CRecord = $CSet->get($CSet->name_fr->like($row->country));

        if (!$CRecord) {
            $country = 0;
        } else {
            $country = $CRecord->id;
        }



        if ($this->country !== $country) {
            $up_prop++;
            $this->country = $country;
        }

        return $up_prop;
    }

}
