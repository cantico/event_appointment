<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));


/**
 * A ea_CountrySet
 */
class ea_CountrySet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('code', 2)
                    ->setDescription('ISO code'),
            ORM_StringField('name_en', 255)
                    ->setDescription('Name (english)'),
            ORM_StringField('name_fr', 255)
                    ->setDescription('Name (french)')
        );
    }


    /**
     *
     *
     * @return string
     */
    public function getNameColumn()
    {

        $colname = 'name_en';
        if ('fr' === $GLOBALS['babLanguage']) {
            $colname = 'name_fr';
        }

        return $colname;
    }/**
     * populate database with geoNames data
     *
     * @return bool
     */
    public function populateFromGeoNames()
    {
        $gn = @bab_functionality::get('GeoNames');
        if (!$gn) {
            return false;
        }

        $iterator = $gn->getCountryOrmSet()->select();

        foreach($iterator as $country) {
            $record = $this->newRecord();
            $record->geoNamesMap($country);
            $record->save();
        }

        return true;
    }

}

/**
 * A ea_Country
 */
class ea_Country extends ORM_MySqlRecord
{


    /**
     * Name of country according to language
     *
     * @return string
     */
    public function getName()
    {
        $colname = 'name_en';

        if ('fr' === $GLOBALS['babLanguage']) {
            $colname = 'name_fr';
        }

        return $this->$colname;
    }

    /**
     * Map a geonames country into a CRM contry record
     *
     * @param	ORM_Record	$geoNameCountry
     */
    public function geoNamesMap(ORM_Record $geoNameCountry)
    {
        $this->code = $geoNameCountry->iso;
        $this->name_en = $geoNameCountry->country_en;
        $this->name_fr = $geoNameCountry->country_fr;
    }
}
