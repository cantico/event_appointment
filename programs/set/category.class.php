<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));




/**
 *
 * @property ORM_StringField					$name
 * @property ORM_StringField					$color
 *
 */
class ea_CategorySet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_StringField('color'),
            ORM_StringField('identifier')
        );
    }

}




/**
 *
 * @property ORM_StringField					$name
 * @property ORM_StringField					$color
 *
 */
class ea_Category extends ORM_MySqlRecord
{
    /**
     * Saves the record.
     *
     * @return boolean True on success, false otherwise
     */
    public function save()
    {
        if($this->identifier == ''){
            $this->identifier = abs(crc32(time()));
        }
        return parent::save();
    }
}