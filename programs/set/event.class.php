<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));




/**
 *
 * @property ORM_StringField					$name
 * @property ORM_StringField					$color
 * @property ORM_TextField						$description
 * @property ORM_DatetimeField					$start
 * @property ORM_DatetimeField					$end
 * @property ORM_IntField						$min_slot
 * @property ORM_IntField						$max_slot
 * @property ORM_BoolField						$signup
 * @property ORM_DateField						$limit_signup
 * @property ORM_DecimalField					$price
 * @property ORM_UserField						$author
 * @property ORM_UserField						$internal_contact
 * @property ORM_StringField					$external_contact
 *
 */
class ea_EventSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_TextField('description'),
            ORM_DatetimeField('start'),
            ORM_DatetimeField('end'),
            ORM_IntField('min_slot'),
            ORM_IntField('max_slot'),
            ORM_BoolField('signup'),
            ORM_DateField('limit_signup'),
            ORM_DecimalField('price', 2),
            ORM_UserField('author'),
            ORM_UserField('internal_contact'),
            ORM_StringField('external_contact')
        );

        $this->hasOne('address', 'ea_addressSet');
        $this->hasOne('category', 'ea_CategorySet');
        $this->hasOne('type', 'ea_TypeSet');
    }
}




/**
 *
 * @property ORM_StringField					$name
 * @property ORM_StringField					$color
 * @property ORM_TextField						$description
 * @property ORM_DatetimeField					$start
 * @property ORM_DatetimeField					$end
 * @property ORM_IntField						$min_slot
 * @property ORM_IntField						$max_slot
 * @property ORM_BoolField						$signup
 * @property ORM_DateField						$limit_signup
 * @property ORM_DecimalField					$price
 * @property ORM_UserField						$author
 * @property ORM_UserField						$internal_contact
 * @property ORM_StringField					$external_contact
 *
 */
class ea_Event extends ORM_MySqlRecord
{

}