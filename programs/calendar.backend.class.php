<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/set/category.class.php';
require_once dirname(__FILE__) . '/set/address.class.php';
require_once dirname(__FILE__) . '/set/event.class.php';

bab_functionality::includefile('CalendarBackend');

class Func_CalendarBackend_eventappointment extends Func_CalendarBackend
{

    public function getDescription()
    {
        require_once dirname(__FILE__).'/functions.php';
        return ea_translate('Read only calendar visualisation for event appointment application');
    }

    /**
     * The backend can be used as a storage backend for the existing calendars (personal only for now)
     * @return bool
     */
    public function StorageBackend()
    {
        return false;
    }


    public function includeEventCalendar()
    {
        parent::includeEventCalendar();
        require_once dirname(__FILE__).'/calendar.class.php';
    }

    /**
     * @return ea_EventCalendar
     */
    public function PublicCalendar()
    {
        $this->includeEventCalendar();
        return new ea_EventCalendar;
    }





    private static function flattenCriteria(bab_PeriodCriteria $criteria, &$criteriaArray)
    {
        $criteriaArray[] = $criteria;
        $subCriteria = $criteria->getCriterions();
        foreach($subCriteria as $subCriterion) {
            self::flattenCriteria($subCriterion, $criteriaArray);
        }
    }


    /**
     * Select periods from criteria
     * the bab_PeriodCriteriaCollection and bab_PeriodCriteriaCalendar are mandatory
     *
     *
     * @param bab_PeriodCriteria $criteria
     *
     * @return iterator <bab_CalendarPeriod>
     */
    public function selectPeriods(bab_PeriodCriteria $criteria)
    {
        $criteriaArray = array();
        self::flattenCriteria($criteria, $criteriaArray);

        $selectedCalendars = array();
        $filteredProperties = array();
        $uid = null;

        $begin = null;
        $end = null;

        foreach ($criteriaArray as $criterion) {
            //			echo get_class($criterion) . "\n";
            if ($criterion instanceof bab_PeriodCriteriaCalendar) {
                $selectedCalendars = array();
                foreach ($criterion->getCalendar() as $calendarId => $calendar) {
                    if ($calendar instanceof ea_EventCalendar) {
                        $selectedCalendars[$calendarId] = $calendar;
                    }
                }
            } else if ($criterion instanceof bab_PeriodCritieraBeginDateLessThanOrEqual) {
                $end = $criterion->getDate();
                $end = isset($end) ? $end->getIsoDateTime() : null;
            } else if ($criterion instanceof bab_PeriodCritieraEndDateGreaterThanOrEqual) {
                $begin = $criterion->getDate();
                $begin = isset($begin) ? $begin->getIsoDateTime() : null;
            } else if ($criterion instanceof bab_PeriodCritieraProperty) {

                foreach ($criterion->getValue() as $value) {
                    $filteredProperties[] = array(
                        'name' => $criterion->getProperty(),
                        'value' => $value,
                        'contain' => $criterion->getContain()
                    );
                }
            } elseif ($criterion instanceof bab_PeriodCritieraUid) {
                $uid = $criterion->getUidValues();
            }
        }

        $periods = array();

        foreach ($selectedCalendars as $calendar) {
            $collection = $this->CalendarEventCollection($calendar);
            $events = $this->getTableEvents($calendar, $collection, $periods, $begin, $end, $filteredProperties, $uid);
        }

        return $periods;
    }




    /**
     * Returns the period corresponding to the specified identifier
     * this is necessary for all events with a link
     *
     * @param	bab_PeriodCollection	$periodCollection		where to search for event, the collection can be populated with the selected event
     * @param 	string 					$identifier				The UID property of event
     * @param	string					[$dtstart]				The DTSTART value of the event (this can be usefull if the event is a recurring event, DTSTART will indicate the correct instance)
     *
     * @return bab_CalendarPeriod
     */
    public function getPeriod(bab_PeriodCollection $periodCollection, $identifier, $dtstart = null)
    {
        $eventSet = ea_EventSet();
        $eventSet->address();
        $eventSet->category();
        $identifier = str_replace('EA-','', $identifier);

        if(!ea_getAccessibleEvents($identifier)){
            throw new Exception(ea_translate('Access denied'));
        }

        $event = $eventSet->get($eventSet->id->is($identifier));

        $start = BAB_DateTime::fromIsoDateTime($event->start);
        $end = BAB_DateTime::fromIsoDateTime($event->end);

        $period = new bab_CalendarPeriod();

        $period->setDates($start, $end);

        $period->setProperty('UID', 'EA-' . $event->id);
        $period->setUiIdentifier('ea_'.$event->category->identifier.'_'.$event->id);
        $period->setProperty('SUMMARY', $event->name);

        $period->setProperty('DESCRIPTION', $event->description);
        $period->setData(
            array(
                'description' => $event->description."<br />".
                    '<a href="'.ea_Controller()->Event(true)->eventView($event->id, true)->url().'">'.ea_translate('Inscription here').'</a>',
                'description_format' => 'html'
            )
        );
        $period->setProperty('LOCATION', $event->address->city);
        $period->setProperty('CATEGORIES', $event->category->name);

        $period->setProperty('X-CTO-COLOR' ,$event->category->color);

        if(bab_isAccessValid('ea_access_groups', 1) || bab_isUserAdministrator()){
            $period->setData(array('id_creator'=>$event->author));
        }

        $period->setProperty('TRANSP', 'OPAQUE');

        $period->setProperty('LAST-MODIFIED', $start->getICal());

        if ($event->internal_contact) {
            $name = bab_getUserName($event->internal_contact);
        } elseif($event->external_contact) {
            $name = $event->external_contact;
        } else {
            $name = '';
        }

        $period->setProperty('ORGANIZER;CN='.$name, '');


        return $period;
    }



    /**
     * Query epr table and extract calendar events
     * @param 	ea_EventCalendar 				$calendar
     * @param 	bab_CalendarEventCollection 	$collection
     * @param 	array 							&$periods
     * @param	string							$begin
     * @param	string							$end
     * @param 	array 							$filteredProperties
     * @param	array							$uid
     * @return unknown_type
     */
    private function getTableEvents(ea_EventCalendar $calendar, bab_CalendarEventCollection $collection, &$periods, $begin, $end, $filteredProperties, $uid)
    {
        global $babDB;
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

        $id_table = $calendar->getUid();

        // property cols

        $cols = array();
        $query_filters = array();

        $eventSet = ea_EventSet();
        $eventSet->category();
        $eventSet->address();

        $criteria = new ORM_TrueCriterion;

        if (!empty($filteredProperties)) {
            $filter_criteria = new ORM_FalseCriterion;
            foreach ($filteredProperties as $filteredPropertiy) {
                switch($filteredPropertiy['name']) {
                    case 'SUMMARY':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->name->contains($filteredPropertiy['value']));
                        break;
                    case 'DESCRIPTION':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->description->contains($filteredPropertiy['value']));
                        break;
                    case 'LOCATION':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->address->city->contains($filteredPropertiy['value']));
                        break;
                    /*case 'DTSTART':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->start->contains($filteredPropertiy['value']));
                        break;
                    case 'DTEND':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->end->contains($filteredPropertiy['value']));
                        break;
                    case 'START_DATE':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->start->contains($filteredPropertiy['value']));
                        break;
                    case 'END_DATE':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->end->contains($filteredPropertiy['value']));
                        break;*/
                    case 'CATEGORIES':
                        $filter_criteria = $filter_criteria->_OR_($eventSet->category->name->contains($filteredPropertiy['value']));
                        break;
                }
            }
            $criteria = $criteria->_AND_($filter_criteria);
        }

        if (null !== $begin)
        {
            $criteria = $criteria->_AND_($eventSet->end->greaterThan($begin));
        }

        if (null !== $end)
        {
            $criteria = $criteria->_AND_($eventSet->start->lessThan($end));
        }

        if (null !== $uid)
        {
            $criteria = $criteria->_AND_($eventSet->id->in($uid));
        }

        $criteria = $criteria->_AND_($eventSet->category->identifier->is($id_table));
        $criteria = $criteria->_AND_($eventSet->category->id->in(ea_getAccessibleCategories()));

        $events = $eventSet->select($criteria);

        //bab_debug($events->getSelectQuery());

        foreach ($events as $event)
        {
            $dtstart = BAB_DateTime::fromIsoDateTime($event->start);
            $dtend = BAB_DateTime::fromIsoDateTime($event->end);

            // create calendar period

            $period = $this->CalendarPeriod(0,0);
            $period->setDates($dtstart, $dtend);

            $period->setProperty('UID', 'EA-' . $event->id);
            $period->setUiIdentifier('ea_'.$id_table.'_'.$event->id);
            $period->setProperty('SUMMARY', $event->name);

            $period->setProperty('DESCRIPTION', $event->description);
            /*$period->setData(
                array(
                    'description' => $event->description."<br />".
                        '<a href="'.ea_Controller()->Event(true)->eventView($event->id)->url().'">'.ea_translate('Inscription here').'</a>',
                    'description_format' => 'html'
                )
            );*/
            $period->setProperty('LOCATION', $event->address->city);
            $period->setProperty('CATEGORIES', $event->category->name);

            $period->setProperty('X-CTO-COLOR' ,$event->category->color);

            if(bab_isAccessValid('ea_access_groups', 1) || bab_isUserAdministrator()){
                $period->setData(array('id_creator'=>$event->author));
            }

            $period->setProperty('TRANSP', 'OPAQUE');

            $period->setProperty('LAST-MODIFIED', $dtstart->getICal());
            $period->addRelation('PARENT', $calendar);

            $collection->addPeriod($period);
            $periods[] = $period;
        }
    }
}