��    o      �  �         `	     a	     o	     ~	     �	  
   �	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     
     !
     '
  
   /
     :
  )   C
     m
  '   u
     �
     �
     �
     �
     �
     �
     �
     �
     �
  
     
     
   "     -     ;     B     N     ]  	   n  J   x     �     �     �     �     �            	   #     -     J     X     t     }  	   �     �     �     �     �     �  
   �     �     �     �     �       B        W     p     w  "   |     �     �     �  
   �     �     �  
   �                    %     D  "   b     �     �     �     �     �  -        C     _  '   ~  L   �  Q   �  ,   E  H   r  2   �     �  	   �     �          &     6     K  +   f     �     �     �     �  :  �          *     A     X     p     �     �     �     �     �     �  
   �     �                     (     4     @  *   I     t  2   y      �  	   �     �     �     �     �     �     
  #   #     G     ^     u     �     �     �     �     �     �  i   �  
   Z     e     u     �     �     �     �  	   �  /   �     �  )        6     >     D     b     f     j     �  
   �     �     �     �     �  >   �       9   )  &   c     �     �  2   �     �     �     �  
             2     D     S     W     `     u     �  *   �     �  #   �       *   <  -   g  1   �  !   �  !   �  2     Y   >  a   �  0   �  O   +  9   {     �     �     �  1   �               6  8   U     �     �      �  .   �     G      M   ;      +             U   _   o   =   
   ,   /       :       F   T             E       .       ^       R          [   H          9       ?          k   l       W       m   4       &           ]   >   3   h   c              P   Q   8       0   d       7      *       @      Y   L   "       i   `      $       -   6               )   !   g       O   #          \             A   e           	   J   K       n   '   j   (   Z      V   X       <   f   S   b                   5              1   N      B   2          a      C             I   %          D    Access denied Add a Category Add a category Add a sign up user Add a type Add an event Address Administration Administration menu All events Author Category Category list Category rights City Color Comment Complement Contacts Countries database initialization... done Country Define categories and rights assocaited Define event types Delete Description Edit Email End date Event Event appointment Event appointment management Event info Event list Event name Event periode Events Events list Export in XLSX External contact Firstname GPS coordinates (Optionnal, only if the current map location is incorrect) Inscription here Internal contact Joined files Lastname Latitude Limit date to sign up Location Longitude Manage administration access Manage events Manage events subscriptions Managing Map Map event Name No Number of slot maximum Number of slot minimum Organization Path to go Phone Postal code Price Private documents Public documents Read only calendar visualisation for event appointment application Return to the event list Rights Save Set access right for this category Sign up Sign up an other contact Sign up list Sign up me Sign up to the event Sign up user Start date Street Subscribers Subscription The Firstname can not be empty The Lastname can not be empty The category name can not be empty The color name can not be empty The email can not be empty The end date is mandatory The event name can not be empty The phone can not be empty The sign up user has to be attach to an event The start date is mandatory The type name can not be empty There is a comment on this inscription. This will remove this category and all linked event will be unlink, proceed? This will remove this event and all linked sign up user will be deleted, proceed? This will remove this sign up user, proceed? This will remove this type and all linked event will be unlink, proceed? This will remove your participation to this event. Type Type list Unsigned View complete event information View google map View subscriber list Who are the event mangers? Who can sign up to this cetegory of events? Yes You are already in _inscription_successfull_msg_ _unsign_successfull_msg_ Project-Id-Version: event_appointment
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-27 17:24+0100
PO-Revision-Date: 2016-02-08 10:47+0100
Last-Translator: Antoine GALLET <antoine.gallet@cantico.fr>
Language-Team: Cantico <antoine.gallet@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: ea_translate;ea_translate:1,2
X-Generator: Poedit 1.8.1
X-Poedit-SearchPath-0: programs
 Accès refusé Ajouter une catégorie Ajouter une catégorie Ajouter une inscription Ajouter un type Ajouter un événement Adresse Administration Menu d'administration Tous les évènements Auteur Catégorie Liste des catégories Droits de la catégorie Ville Couleur Commentaire Complément Inscrits Initialisation de la base des pays... fait Pays Définir les catégories et leurs droits associés Définir les types d'événement Supprimer Description Modifier Email Date de fin Évènement Événements disponibles Gestion des évènements disponible Info sur l'évènement Liste des événements Nom de l'événement Période de l'événement Événements Liste de évènements Exporter en XLSX Contact externe Prénom Coordonnées GPS (Optionnelles, à renseigner uniquement si le positionnement sur la carte est incorrect) S'inscrire Contact interne Fichiers joints Nom Latitude Date limite d'inscription Localisation Longitude Gestion des droits d'accès à l'administration Gestion des événements Gestion de l'inscription aux événements Gestion Carte Localisation de l'événement Nom Non Nombre maximum d'inscrits Nombre minimum d'inscrits Entreprise Comment y aller ? Téléphone Code postal Prix Documents privés (réservé aux personnes pouvant s'inscrire) Documents publics Visualisation dans l'agenda pour le module d'évènements Retourner à la liste des évènements Droits Enregistrer Configurer les droits d'accès de cette catégorie Inscription Inscrire une autre personne Liste des inscrits M'inscrire S'inscrire à l'évènement Personne inscrite Date de début Rue Inscrits Gestion des inscrits Le prénom ne peut être vide. Le nom ne peut être vide. Le nom de la catégorie ne peut être vide La couleur doit être définie L'adresse email ne peut être vide. La date de fin est obligatoire Le nom de l'événement ne peut être vide Le numéro de téléphone ne peut être vide. L'inscrit doit être attachée à un événement. La date de début est obligatoire Le nom du type ne peut être vide Un commentaire est présent sur cette inscription. Cela va supprimer cette catégorie et tous les événements associés seront conservés ? Cela va supprimer cette événement et tous les inscrits associés seront également supprimés ? Cela va supprimer cette inscription, continuer ? Cela va supprimer ce type et tous les événement associés seront conservés ? Cela va supprimer votre participation à cet évènement. Type Liste des types Me désinscrire Voir les informations complètes de l'événement Lien googlemap Voir la liste des inscrits Gestionnaires des événements Qui peut s'inscrire aux événements de cette catégorie Oui Vous êtes déjà inscrit. Votre inscription est confirmée Votre désinscription a été prise en compte. 